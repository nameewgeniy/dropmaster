<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'name',
        'country',
        'stars',
        'comment',
        'images',
        'product_id',
        'date'
    ];

    public static function newOrder($data)
    {
        $order = Order::create([
            'client'    => json_encode($data['client']),
            'products'  => json_encode($data['products']),
            'price'     => $data['final_price'] . Setting::getCurrency(),
            'promocode' => isset($data['promocode']) ? $data['promocode'] : null,
            'key'       => strtoupper(substr(md5($data['client']['email'] . time()),0, 10)),
            'payment_id'=> null,
            'status'    => 'pending payment'
        ]);
        return $order->id;
    }
}
