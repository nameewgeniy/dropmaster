<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = array('name', 'code', 'value', 'end_date');

    /**
     * Получаем rate купона для
     * применения его в корзине
     * @param $code
     * @return float|int
     */
    public static function getSaleByCode($code)
    {
        $coupon = self::whereCode($code);
        if (!is_null($coupon->first())){
            $value = $coupon->first()->value;
            if($value){
                return $value/100;
            }
            return 0;
        }else{
            return 0;
        }
    }

}
