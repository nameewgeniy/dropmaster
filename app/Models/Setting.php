<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 * @package App\Models
 */
class Setting extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'value'
    ];

    /**
     * @param $name
     * @param string $default
     * @return string
     */
    public static function getSetting($name, $default='')
    {
        $setting = Setting::whereName($name)->first();
        if( !is_null($setting) ){
            return $setting->value;
        }else{
            return $default;
        }
    }

    /**
     * @return string
     */
    public static function getAdminEmail()
    {
        return self::getSetting('email');
    }

    /**
     * @return string
     */
    public static function getGallery()
    {
        $imagesText = self::getSetting('gallery');
        if($imagesText)
            return explode(',',$imagesText);
        else
            return null;
    }

    /**
     * @return string
     */
    public static function getCurrency()
    {
        $setting = Setting::whereName('currency')->first();

        if(!is_null($setting))
        {
            switch ($setting->value) {
                case '0':
                    return '$';
                    break;

                case '2':
                    return '€';
                    break;

                case '1':
                    return 'RUB';
                    break;

                case '3':
                    return '£';
                    break;

                case '4':
                    return 'C$';
                    break;

                default:
                    return $setting->value;
                    break;
            }
        }else{
            return '';
        }

    }

    /**
     * @return string
     */
    public static function getCurrencyCode()
    {
        $setting = Setting::whereName('currency')->first();

        if(!is_null($setting))
        {
            switch ($setting->value) {
                case '0':
                    return 'USD';
                    break;

                case '2':
                    return '€';
                    break;

                case '1':
                    return 'RUB';
                    break;

                case '3':
                    return '£';
                    break;

                case '4':
                    return 'CAD';
                    break;

                default:
                    return $setting->value;
                    break;
            }
        }else{
            return '';
        }

    }
}
