<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'client',
        'products',
        'promocode',
        'status',
        'price',
        'key',
        'payment_id',
        'payer_id'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany('App\Models\Sale');
    }

    /**
     * Создаём новую покупку
     * @param $data
     * @return mixed
     */
    public static function newOrder($data)
    {
        $order = self::create([
            'client'    => json_encode($data['client']),
            'price'     => $data['final_price'] . Setting::getCurrency(),
            'promocode' => isset($data['promocode']) ? $data['promocode'] : null,
            'key'       => strtoupper(substr(md5($data['client']['email'] . time()),0, 10)),
            'payment_id'=> null,
            'status'    => 'pending payment'
        ]);

        $order->setSales($data['products']);

        return $order->id;
    }

    /**
     * Запоминаем проданные товары
     * @param $products
     * @return $this
     */
    public function setSales($products)
    {
        /* Привязываем нужные товары */
        if (is_array($products)){

            $sales = null;

            foreach($products as $sale){

                $sales[] = [
                    'product_id' => $sale['product_id'],
                    'name'       => Product::whereId($sale['product_id'])->pluck('title')->first(),
                    'variation'  => $sale['name'],
                    'count'      => $sale['quantity'],
                    'price'      => $sale['price'],
                    'track'      => 'expected',
                    'img'        => $sale['img'],
                    'sku'        => $sale['sku'],
                    'currency'   => $sale['currency']
                ];

            }
            if(!is_null($sales)){

                $this->sales()->createMany($sales);
            }
        }
        return $this;
    }

}
