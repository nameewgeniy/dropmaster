<?php

namespace App\Models;

use App\Http\Controllers\Helpers\Functions;
use Illuminate\Database\Eloquent\Model;
use Baum\Node;

class Category extends Node
{
    protected $table = 'categories';
    protected $fillable = array('name', 'slug', 'parent_id');

    /**
     *
     */
    public static function getChildren()
    {
        $items = new Category();
        //dd($items->whereId(1)->first()->getDescendantsAndSelf());
        $items->whereId(1)->first()->getDescendants();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function createCategoryByName($name)
    {
        if(Category::whereName($name)->count() > 0)
        {
            $category = Category::updateOrCreate(['name' => $name],[
                'name' => $name,
                'slug' => Functions::generateSlug($name)
            ]);
        }else{
            $category = Category::create([
                'name' => $name,
                'slug' => Functions::generateSlug($name)
            ]);
        }
        return [ $category->id ];
    }

    /**
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getCategoriesExcludeBySlug($slug)
    {
        if(!is_array($slug)){
            $slug = [ $slug ];
        }
        return Category::whereNotIn('slug', $slug)->get();
    }


}
