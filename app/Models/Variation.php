<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'img',
        'sku',
        'price',
        'count',
        'check',
        'name',
        'sale'
    ];

    /**
     * Формируем массив продуктов из корзины
     * @param array $request
     * @return array
     */
    public static function getItemsFromCart($request = [])
    {
        $items = [];

        if (isset($request['items'])){

            $items = $request['items'];

        }elseif (isset($_COOKIE['cart']) &&  $_COOKIE['cart'] != null ){

            $items = json_decode($_COOKIE['cart'], true);
        }

        if (!empty($items)){

            $items_cart  = [];
            $final_price = 0;
            $final_count = 0;
            $discount = 0.00;

            $variations = Variation::whereIn('id' , array_keys($items))->get()->toArray();

            foreach($variations as $variation)
            {
                $items_cart[] = array_merge($variation, [
                    'quantity' => $items[ $variation['id'] ],
                    'currency' => Setting::getCurrency(),
                    'track_id' => 'expected',
                    'details'  => 'no'
                ]);

                /* Расчитываем конечную стоимость */
                $final_price += $variation['price'] * $items[ $variation['id'] ];

                /* Расчитываем общее количество*/
                $final_count += $items[ $variation['id'] ];
            }

            if(isset($request['promocode']))
            {
                /* Здесь дёргаем промокод и применяем рате */
                $rate = Coupon::getSaleByCode($request['promocode']);
                $discount       = $final_price * $rate;
                $final_price    = $final_price - $final_price * $rate;
            }

            return [
                'products'      => $items_cart,
                'final_price'   => round($final_price, 2),
                'final_count'   => $final_count,
                'discount'      => round($discount, 2)
            ];
        }else{
            return [];
        }

    }
}
