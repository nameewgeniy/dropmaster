<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class Image
 * @package App\Models
 */
class Image extends Model
{
    /**
     * @param $url
     * @return bool
     */
    public function uploadFromURL($url)
    {
        if(is_array($url))
        {
            $ids = [];
            foreach($url as $link)
            {
                if(!empty($link)){
                    $ids[] = $this->uploadImage($link);
                }
            }
            return $ids;
        }else
        {
            return $this->uploadImage($url);
        }
    }

    public static function generateImagesSize($path)
    {
        $img = \Intervention\Image\Facades\Image::make($path);
        $img->fit(500, 500)->save($path.'.500x500.jpg');
        $img->fit(220, 220)->save($path.'.220x220.jpg');
        $img->fit(50, 50)->save($path.'.50x50.jpg');
    }

    public static function getImageBySrc($src, $size)
    {
        return url( config('filesystems.disks.images.url') . '/'. $src. '.' . $size . '.jpg');
    }

    public static function getImageByID($id, $size = '220x220')
    {
        $image = Image::find($id);
        if(isset($image->src))
        {
            if($size == 'full' || $size == '')
            {
                return url( config('filesystems.disks.images.url') . '/'. $image->src);
            }else{
                return url( config('filesystems.disks.images.url') . '/'. $image->src. '.' . $size . '.jpg');
            }
        }else{
            return null;
        }
    }

    /**
     * @param $link
     * @return bool
     */
    private function uploadImage($link)
    {
        $fl     = file_get_contents($link);
        $name   = uniqid('thm_', true) . '.jpg';
        if(Storage::disk('images')->put( $name, $fl))
        {
            Image::generateImagesSize(config('filesystems.disks.images.root').'/'. $name );
            $images = new Image();
            $images->src = $name;
            $images->save();
            return $images->id;
        }else
        {
            return false;
        }
    }
}
