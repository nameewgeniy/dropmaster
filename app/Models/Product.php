<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Product
 * @package App\Models
 */
class Product extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'text',
        'title_seo',
        'slug',
        'price',
        'keywords',
        'description',
        'rate',
        'status',
        'user_id',
        'rate',
        'orders',
        'link',
        'user_name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany('App\Models\Image');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany('App\Models\Option');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function variations()
    {
        return $this->hasMany('App\Models\Variation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public static function createOrUpdate($request)
    {
        $product = self::updateOrCreate(['id' => $request['id'] ],[
            'title'         => $request['title'],
            'text'          => $request['content'],
            'keywords'      => isset($request['keywords'])? $request['keywords'] : '',
            'description'   => isset($request['description'])? $request['description'] : '',
            'title_seo'     => $request['title_seo'],
            'status'        => $request['status'],
            'link'          => isset($request['link']) ? $request['link'] : null,
            'slug'          => str_slug($request['slug']),
            'orders'        => isset( $request['orders'])? $request['orders'] : rand(100, 500),
            'rate'          => isset( $request['rate'])? $request['rate'] : rand(4,5),
            'user_name'     => Auth::user()->name,
            'user_id'       => Auth::user()->id,
        ]);

        $product->setImages($request)
            ->setCategories($request)
            ->setOptions($request)
            ->setVariations($request)
            ->setCommentaries($request);

        return $product;
    }

    /**
     * @param $request
     * @return $this
     */
    public function setImages($request)
    {
        if (isset($request['images'])){

            /* Отвязывем все изображения */
            $this->images()->detach();

            /* Привязываем нужные изображение */
            if (isset($request['images'])){

                foreach ($request['images'] as $imgID)
                {
                    $this->images()->attach($imgID);
                }
            }
        }
        return $this;
    }

    /**
     * @param $request
     * @return $this
     */
    public function setCommentaries($request)
    {
        if (isset($request['comments'])){

            $comments = null;

            foreach($request['comments'] as $comment){

                $comments[] = [
                    'name'      => $comment['name'],
                    'country'   => $comment['country'],
                    'date'      => $comment['date'],
                    'comment'   => $comment['comment'],
                    'stars'     => $comment['stars'],
                    'images'    => isset($comment['images'])? json_encode($comment['images']) : null
                ];
            }

            if(!is_null($comments)){

                $this->comments()->delete();
                $this->comments()->createMany($comments);
            }
        }
        return $this;
    }

    /**
     * @param $request
     * @return $this
     */
    public function setCategories($request)
    {
        /* Привязываем нужные категории */
        if (isset($request['categories'])){

            /* Отвязываем все категории */
            $this->categories()->detach();

            foreach ($request['categories'] as $catID){
                $this->categories()->attach($catID);
            }
        }
        return $this;
    }

    /**
     * @param $request
     * @return $this
     */
    public function setVariations($request)
    {
        $variations = null;
        /* Привязываем нужные вариации, если они существуют */
        if (isset($request['variations']) && !is_null($request['variations'])){

            foreach($request['variations'] as $variation){
                $variations[] = [
                    'img'       => isset($variation['img'])? $variation['img'] : null ,
                    'sku'       => $variation['sku'],
                    'check'     => isset($variation['check'])? $variation['check'] : 'on',
                    'price'     => $variation['price'],
                    'count'     => $variation['count'],
                    'name'      => $variation['name']
                ];
            }

        }elseif (isset($request['default'])){ // если вариаций нет, то записываем дефолтные значения для товара без опций

            $variations[] = [
                'img'       => null ,
                'sku'       => 'default',
                'check'     => 'on',
                'price'     => $request['default']['price'],
                'count'     => $request['default']['count'],
                'name'      => 'default',
            ];
        }

        if (!is_null($variations)){
            $this->variations()->delete();
            $this->variations()->createMany($variations);
        }

        return $this;
    }

    /**
     * @param $request
     * @return $this
     */
    public function setOptions($request)
    {
        /* Привязываем нужные опции */
        if (isset($request['options']) && !is_null($request['options'])){

            $options = null;
            $image = new Image();

            foreach($request['options'] as $option){

                if (!is_array($option['values'])){
                    $option['values'] = explode(',', $option['values']);
                }

                foreach($option['values'] as $value){
                    $options[] = [
                        'name'  => $option['name'],
                        'value' => isset($value['value'])? $value['value'] : $value,
                        'img'   => ( isset($value['img']) && !empty($value['img']) )?
                                $image->uploadFromURL($value['img']) : null
                    ];
                }

            }
            if(!is_null($options)){

                $this->options()->delete();
                $this->options()->createMany($options);
            }
        }
        return $this;
    }

    /**
     * @param $options
     * @return array
     */
    public function renderOptions($options)
    {
        $result = [];
        foreach($options as $option){
            $result[trim($option['name'])]['values'][] = $option['value'];
            $result[trim($option['name'])]['obj'][] = $option;
        }
        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getFirstImg($id)
    {
        $product = Product::find($id);
        if(!empty($product->images)){
            return $product->images()->first()->id;
        }
        return 0;
    }

    /**
     * @param $type
     * @return array
     */
    public function getVariations($type = ''){

        if ($type == 'default'){
            $default = $this->variations()->whereSku('default')->get()->toArray();
            if(is_array($default)){
                return array_shift($default);
            }else{
                return [];
            }
        }else{
            return $this->variations()->where('sku', '<>', 'default')->get()->toArray();
        }
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->variations()->first()->price;
    }

    /**
     * @return string
     */
    public function getSalePrice()
    {
        $sale = $this->variations()->first()->sale;
        return is_null($sale)? '0.00' : $sale;
    }

    /**
     * @return mixed
     */
    public function getBestPrice()
    {
        return $this->variations()->min('price');
    }

    /**
     * Продукты из категории текущего товара
     * исключая bestsellers
     * @return mixed
     */
    public function getProductsOfCategoryProduct()
    {
        if(isset($this->categories)){

            // Категории без bestsellers
            $categories = $this->categories->filter(function ($value, $key) {
                return $value['slug'] != 'bestsellers';
            });

            return $categories->shift()->products->take(4);
        }else{
            Category::whereSlug('bestsellers')->first()->products->take(4);
        }
    }
}
