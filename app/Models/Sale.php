<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'order_id',
        'product_id',
        'name',
        'variation',
        'count',
        'price',
        'track',
        'img',
        'sku',
        'currency',
        'details'
    ];

    protected $dates = ['deleted_at'];
}
