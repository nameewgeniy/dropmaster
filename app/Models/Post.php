<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'text', 'user_name', 'user_id', 'thumbnail_id', 'keywords', 'description', 'h1', 'status' ];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

}
