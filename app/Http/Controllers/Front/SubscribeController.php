<?php namespace App\Http\Controllers\Front;

use App\Models\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SubscribeController extends Controller {

    /**
     * Подписка на рассылку
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request )
    {
        /* Здесь сделать сохранение в базу имейла */
        $validator = Validator::make($request->all(),[ 'subscribe' => 'unique:subscribes,email|required|email' ]);

        if($validator->passes()){
            $subscribe              = new Subscribe();
            $subscribe->email       = $request['subscribe'];
            $subscribe->subscribe   = 'active';
            $subscribe->save();
        }else{
            return redirect()->back()->withErrors($validator);
        }

        return view($this->_tempPath . 'subscribe.subscribe');
    }

}
