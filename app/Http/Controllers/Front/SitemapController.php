<?php
/**
 * Created by PhpStorm.
 * User: nakap
 * Date: 21.11.17
 * Time: 15:16
 */

namespace App\Http\Controllers\Front;
use App\Models\Category;
use App\Models\Product;

/**
 * Class SitemapController
 * @package App\Http\Controllers\Front
 */
class SitemapController {

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function sitemapXML(){
        $maps = $this->renderIndexSitemap([ url('/categories.xml'), url('/products.xml') ]);
        return response($maps, 200, ['Content-Type' => 'application/xml;charset=UTF-8']);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function categories(){
        return response($this->renderXMLSitemap(Category::all(), 'category'), 200, ['Content-Type' => 'application/xml;charset=UTF-8']);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function products(){
        return response($this->renderXMLSitemap(Product::all(), 'product'), 200, ['Content-Type' => 'application/xml;charset=UTF-8']);
    }

    /**
     * @param $items
     * @param $type
     * @return string
     */
    private function renderXMLSitemap( $items, $type )
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach( $items as $item ){
            $xml .= '<url>';
            $xml .= '<loc>'. url($type . '/' . $item['slug']) .'</loc>';
            $xml .= '<lastmod>'. date('Y-m-d', strtotime($item['created_at'])) .'</lastmod>';
            $xml .= '<changefreq>weekly</changefreq>';
            $xml .= '<priority>0.6</priority>';
            $xml .= '</url>';
        }
        $xml .= '</urlset>';

        return $xml;
    }

    /**
     * @param $into
     * @return string
     */
    private function renderIndexSitemap($into){
        $xml =  '<?xml version="1.0" encoding="UTF-8"?> ';
        $xml .= '<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
        $xml .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 ';
        $xml .= 'http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" ';
        $xml .= 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> ';
        if(is_array($into))
            foreach($into as $link){
                $xml .= '<sitemap> ';
                $xml .= '<loc>'. $link .'</loc> ';
                $xml .= '<lastmod>'. date('Y-m-d') .'</lastmod> </sitemap>';
            }
        $xml .= '</sitemapindex>';
        return $xml;
    }

} 