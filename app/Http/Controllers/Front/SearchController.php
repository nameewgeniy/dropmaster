<?php

namespace App\Http\Controllers\Front;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'categories'  => Category::getCategoriesExcludeBySlug('bestsellers'),
            'products'    => []
        ];

        if (is_null($this->validator($request))){

            $data['products'] = Product::where('title', 'ILIKE', '%'. $request['skey'] .'%')
                ->orWhere('text', 'ILIKE', '%'. $request['skey'] .'%')
                ->orWhere('keywords', 'ILIKE', '%'. $request['skey'] .'%')
                ->orWhere('description', 'ILIKE', '%'. $request['skey'] .'%')->get();

        }

        return view($this->_tempPath . 'search.search', array_merge($data, $request->all()) );
    }

    public function validator($request)
    {
        $validator = Validator::make($request->all(),[
                'skey' => 'required|string'
        ]);

        if ($validator->fails()){
            return $validator->fails();
        }else{
            return null;
        }
    }
}
