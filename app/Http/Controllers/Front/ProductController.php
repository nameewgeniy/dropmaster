<?php

namespace App\Http\Controllers\Front;

use App\Models\Image;
use App\Models\Product;
use App\Models\Category;
use App\Models\Setting;
use App\Models\Variation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index( $slug )
    {

        if (($product = Product::whereSlug($slug)->first()) == null){
            return redirect('/404');
        }

        $data = [
            'product'       => $product,
            'title'         => $product->title_seo,
            'keywords'      => $product->keywords,
            'description'   => $product->description,
            'related'       => $product->getProductsOfCategoryProduct(),
            'currency'      => Setting::getCurrency(),
            'default'       => $product->getVariations('default'),
            'options'       => $product->renderOptions($product->options()->get()->toArray())
        ];

        return view($this->_tempPath . 'product.product', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function stock( Request $request )
    {
        if($request->ajax())
        {
            /* Добавить валидацию */

            $var = Variation::whereProduct_id($request['productId'])
                ->whereSku($request['sku'])
                ->get()
                ->shift()
                ->toArray();

            // Костыль, чтоб его...
            if(!is_null($var['img'])){

                $src = Image::getImageByID($var['img']);
                $var = array_merge($var,[ 'src' => $src ]);

            }

            return response()->json($var);

        }else{
            return 'Is not ajax';
        }
    }

}
