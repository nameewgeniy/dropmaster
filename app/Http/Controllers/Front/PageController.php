<?php

namespace App\Http\Controllers\Front;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{

    public function order(Request $request)
    {
        $validator = Validator::make($request->all(), ['key' => 'required']);

        if($validator->fails()){
            return view($this->_tempPath . 'pages.order-status' );
        }else{
            return view($this->_tempPath . 'pages.order-status', [
                'order' => Order::where('key',$request['key'])->first(),
                'key'   => $request['key']
            ] );
        }
    }

    public function track()
    {
        return view($this->_tempPath . 'pages.tracking' );
    }

    public function privacyPolicy()
    {
        return view($this->_tempPath . 'pages.privacy-policy' );
    }

    public function termsAndConditions()
    {
        return view($this->_tempPath . 'pages.terms-and-conditions' );
    }

    public function paymentMethods()
    {
        return view($this->_tempPath . 'pages.payment-methods' );
    }

    public function shippingDelivery()
    {
        return view($this->_tempPath . 'pages.shipping-delivery' );
    }

    public function returnsPolicy()
    {
        return view($this->_tempPath . 'pages.returns-policy' );
    }

    public function faq()
    {
        return view($this->_tempPath . 'pages.faq' );
    }

    public function contactUs()
    {
        return view($this->_tempPath . 'pages.contact-us' );
    }

    public function success(Request $request)
    {
        if(isset($request['after']) && isset($request['order']))
            return view($this->_tempPath . 'pages.success-pay', $request->all() );
        else
            return '401';
    }

    public function error(Request $request)
    {
        if(isset($request['after']))
            return view($this->_tempPath . 'pages.error-pay', $request->all() );
        else
            return '401';
    }
}
