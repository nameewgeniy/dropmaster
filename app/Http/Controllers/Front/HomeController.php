<?php

namespace App\Http\Controllers\Front;

use App\Models\Image;
use App\Models\Post;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'products'    => Product::whereStatus(1)->get(),
            'categories'  => Category::getCategoriesExcludeBySlug('bestsellers'),
            'currency'    => Setting::getCurrency(),
            'homeText'    => Post::find(14) // Дёргаем пост по id для вывода на главной
        ];
        return view($this->_tempPath . 'home.home', $data);
    }
}
