<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index($slug, Request $request)
    {
        $data = [
            'sort'        => isset($request['sortBy'])? $request['sortBy'] : 'created_at',
            'category'    => Category::whereSlug($slug)->first(),
            'categories'  => Category::getCategoriesExcludeBySlug('bestsellers'),
        ];

        return view($this->_tempPath . 'category.category', $data);
    }
}
