<?php

namespace App\Http\Controllers\Front;

use App\Mail\ContactUs;
use App\Mail\NewOrder;
use App\Mail\OrderShipped;
use App\Mail\OrderUpdate;
use App\Models\Order;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{
    /**
     * отправляем письмо клиенту инфу о статусу заказа
     * @param int $orderID
     */
    public static function sendOrderInfoToClient($orderID)
    {
        $order  = Order::find($orderID);
        $client = json_decode($order->client, true);

        Mail::to($client['email'])->send(new OrderShipped($order));
        Mail::to(Setting::getAdminEmail())->send(new NewOrder());
    }

    /**
     * отправляем письмо клиенту инфу о изменении статусу заказа
     * @param int $orderID
     */
    public static function sendUpdateOrderInfoToClient($orderID)
    {
        $order  = Order::find($orderID);
        $client = json_decode($order->client, true);

        Mail::to($client['email'])->send(new OrderUpdate($order));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public static function sendContactUs(Request $request)
    {
        $val = Validator::make($request->all(),[
            'name'  => 'required',
            'email' => 'required',
            'text'  => 'required'
        ]);
        if(!$val->fails()){

            Mail::to(Setting::getAdminEmail())->send(new ContactUs($request->all()));
            return redirect('contact-us?send=true');

        }else{
            return redirect()->back()->withErrors($val);
        }

    }
}
