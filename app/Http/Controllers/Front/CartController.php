<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Helpers\Functions;
use App\Models\Variation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Anouar\Paypalpayment\Facades\PaypalPayment;
use App\Models\Setting;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

/**
 * Class CartController
 * @package App\Http\Controllers\Front
 */
class CartController extends Controller
{

    /**
     * @var
     */
    public $_apiContext;


    /**
     * Установка параметров api PayPal
     */
    private function set_api()
    {
        $this->_apiContext = Paypalpayment::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            //'mode' => 'production',
            //'mode' => 'sandbox',
            'mode' => config('services.paypal.mode'),
            'service.EndPoint' => (config('services.paypal.mode') == 'production') ? 'https://api.paypal.com' : 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));

    }

    /**
     * Отображение корзины и вывод всех эдементов
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = [
            'countries'   => Functions::getCountries()
        ];

        return view($this->_tempPath . 'cart.cart', array_merge($data, Variation::getItemsFromCart($request->all())) );
    }

    /**
     * Возвращаем регионы для
     * заказа товара
     * @param Request $request
     * @return array
     */
    public function state(Request $request)
    {
        $states = [];
        $storage = Storage::disk('state');

        if ($storage->has('countries.json') && isset($request['code'])){

            $filename = collect( json_decode($storage->get('countries.json')) )
                ->where('code', $request['code'])->pluck('filename')->first();

            if(!is_null($filename) && $storage->has('countries/' . $filename . '.json')){
                $states = json_decode($storage->get('countries/' . $filename . '.json'));
            }

        }
        return response()->json($states);
    }

    /**
     * Создание ордера и платежа
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function pay(Request $request)
    {
        $val = $this->validatorClient($request->all());

        if(!$val->fails()){

            $items = Variation::getItemsFromCart($request->all());
            $orderId = Order::newOrder(array_merge($request->all(), $items));

            return $this->createPay($items, array_merge($request->all(), [ 'orderId' => $orderId ]) );
        }else{
            return redirect()->back()->withErrors($val);
        }

    }

    /**
     * Колбек после создания платежа
     * @param Request $request
     * @return string
     */
    public function callback(Request $request)
    {
        $val = $this->validator($request->all());

        if(!$val->fails()){

            $order = Order::find($request['order']);
            $args  = [
                'payment_id'    => $request['paymentId'],
                'payer_id'      => $request['PayerID']
            ];
            $url = 'error?after=true';

            if($request['success'] == 'true'){
                try{

                    $order->update(array_merge($args, ['status' => 'paid']));
                    $this->execute($request['paymentId'], $request['PayerID']);

                    $url = 'success?after=true&order=' . $order['key'];

                }catch (\Exception $e){

                    $order->update(array_merge($args, ['status' => 'code error']));

                    $url = 'error?after=true';
                }
            }else{

                $order->update(array_merge($args, ['status' => 'payment error']));

                $url = 'error?after=true';
            }

            MailController::sendOrderInfoToClient($request['order']);
            return redirect($url);

        }else{
            return redirect('error?after=true');
        }
    }

    /**
     * Создание платежа PayPal
     * @param $items
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function createPay($items, $request)
    {
        $this->set_api();

        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("paypal");

        $itemList = Paypalpayment::itemList();
        $itemList->setItems($this->createListProducts($items));
            //->setShippingAddress($this->createShippingAddress($request));

        $amount = Paypalpayment::amount();
        $amount->setCurrency(Setting::getCurrencyCode())
            ->setTotal( $items['final_price']);

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setInvoiceNumber(uniqid());

        $redirectUrls = Paypalpayment::redirectUrls();
        $redirectUrls->setReturnUrl(url("callback?success=true&order=" . $request['orderId']))
            ->setCancelUrl(url("callback?success=false&order=" . $request['orderId']));

        $payment = Paypalpayment::payment();

        $payment->setExperienceProfileId($this->createWebProfile());

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $payment->create($this->_apiContext);

        if($payment->getApprovalLink())
        {
            return redirect()->away($payment->getApprovalLink());
        }else{
            $this->execute($payment->id, 'nameewgeniy@ya.ru');
        }
    }

    /**
     * Кастомизируем страницу оплаты
     * @return mixed
     */
    public function createWebProfile(){

        $flowConfig = Paypalpayment::FlowConfig();
        $presentation = Paypalpayment::Presentation();
        $inputFields = Paypalpayment::InputFields();
        $webProfile = Paypalpayment::WebProfile();
        $flowConfig->setLandingPageType("Billing");

        $presentation->setLogoImage(url('logo.jpg'))
            ->setLocaleCode('US')
            ->setBrandName(config('app.name'));

        $inputFields->setAllowNote(true)->setNoShipping(0)->setAddressOverride(0);

        $webProfile->setName(config('app.name') . uniqid())
            ->setFlowConfig($flowConfig)
            // Parameters for style and presentation.
            ->setPresentation($presentation);
            // Parameters for input field customization.
            //->setInputFields($inputFields);

        $createProfileResponse = $webProfile->create($this->_apiContext);

        return $createProfileResponse->getId();
    }

    /**
     * Формируем массив объектов продукта
     * для оплаты
     * @param $items
     * @return array
     */
    private function createListProducts($items)
    {
        $pay_items = [];

        if(isset($items['products'])){
            foreach($items['products'] as $item){

                $pay_item = Paypalpayment::item();
                $pay_items[] = $pay_item->setName(Product::whereId($item['product_id'])->pluck('title')->first())
                    ->setDescription($item['name'])
                    ->setCurrency(Setting::getCurrencyCode())
                    ->setQuantity($item['quantity'])
                    ->setPrice($item['price']);
            }
        }

        if( $items['discount'] > 0 ){
            $discount = Paypalpayment::item();
            $pay_items[] = $discount->setName('Discount')
                ->setCurrency(Setting::getCurrencyCode())
                ->setQuantity(1)
                ->setPrice( - $items['discount']);
        }

        return $pay_items;
    }

    /**
     * Формируем объект адреса доставки
     * @param $request
     * @return mixed
     */
    private function createShippingAddress($request)
    {
        $shippingAddress = Paypalpayment::shippingAddress();
        $shippingAddress->setLine1($request['client']['address'])
            ->setLine2($request['client']['etc'])
            ->setCity($request['client']['city'])
            ->setState($request['client']['region'])
            /*->setPostalCode($request['client']['code'])*/
            ->setCountryCode($request['client']['country'])
            ->setPhone('9 (385) 122-51-0')
            ->setRecipientName($request['client']['first_name']);

        return $shippingAddress;
    }

    /**
     * Запус процесса оплаты и снятия денег
     * @param $paymentId
     * @param $PayerID
     */
    public function execute($paymentId, $PayerID)
    {
        $this->set_api();

        $payment = Paypalpayment::getById($paymentId, $this->_apiContext);

        $execution = Paypalpayment::PaymentExecution();
        $execution->setPayerId($PayerID);

        //Execute the payment
        $payment->execute($execution,$this->_apiContext);
    }

    /**
     * Валидатор входящих данных в колбек
     * @param $request
     * @return mixed
     */
    private function validator($request){

        $validator = Validator::make($request, [
            //'paymentId'    => 'required',
            //'PayerID'      => 'required',
            'success'      => 'required'
        ]);

        return $validator;
    }

    /**
     * Валидатор входящих данных клиента
     * @param $request
     * @return mixed
     */
    private function validatorClient($request){

        $validator = Validator::make($request, [
            'client.email'     => 'required',
            'client.name'      => 'required',
            'client.address'   => 'required',
            'client.city'      => 'required',
            'client.country'   => 'required',
            'client.region'    => 'required'
        ]);

        return $validator;
    }

}
