<?php namespace App\Http\Controllers\Front;

use App\Models\Product;
use App\Models\Setting;
use Illuminate\Http\Request;
use Exception;
use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\Image;

class FeedController extends Controller {

    public function feed( $slug, Request $request ){

        $category = Category::whereSlug($slug)->first();

        return response($this->renderRSS($category->products), 200, ['Content-Type' => 'application/xml;charset=UTF-8']);
    }

    private function renderRSS( $data ){

        $rss = "<rss xmlns:cf=\"http://www.microsoft.com/schemas/rss/core/2005\" xmlns:e=\"http://www.ebay.com/marketplace/search/v1/services\" version=\"2.0\"><channel><title></title><link>#</link><subtitle>Customize as you please by changing the URL. The keyword before the .atom / .rss extension determines the result that is displayed</subtitle>";

        foreach( $data as $product ){

            $rss .= "<item> <title>" . $product->title . " | " . $product->getPrice().' '. Setting::getCurrency() . " | Best price guarantee!</title>";
            $rss .= "<guid>" . $product->id . " </guid>";
            $rss .= "<link>" . url('product/' . $product->slug) . " </link>";
            $rss .= "<pubDate>" . $product->created_at . " </pubDate>";
            $rss .= "<e:image>" . Image::getImageByID($product->images[0]->id) . " </e:image></item>";

        }

        $rss .= "</channel></rss>";

        return preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $rss);
    }



}
