<?php
/**
 * Created by PhpStorm.
 * User: nakap
 * Date: 01.10.17
 * Time: 12:07
 */

namespace App\Http\Controllers\Helpers;


use Illuminate\Support\Facades\Storage;

class Functions {

    /**
     * @param $value
     * @param null $current
     * @param string $default
     * @return string
     */
    public static function selected($value, $current = null, $default = '')
    {
        if(!is_null($current) && $current == $value)
        {
            return 'selected';
        }else{
            return $default;
        }
    }

    // ограничение выводимых слов
    /**
     * @param string $content
     * @param int $length
     * @return string
     */
    public static function more($content = '', $length = 55)
    {

        if($content) {
            $words = explode(' ', $content, $length + 1);
            if(count($words) > $length) {
                array_pop($words);
                array_push($words, '...');
                $content = implode(' ', $words);
            }
        }
        return $content;
    }

    /**
     * @return mixed
     */
    public static function getCountries()
    {
        /*$lang = 3;
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: remixlang=$lang\r\n"
            )
        );
        $methodUrl = 'http://api.vk.com/method/database.getCountries?v=5.68&need_all=1&count=250';
        $streamContext = stream_context_create($headerOptions);
        $json = file_get_contents($methodUrl, false, $streamContext);
        $respArray = json_decode($json, true);

        if(isset($respArray['response']['items']))
        {
            return $respArray['response']['items'];
        }else{
            return [];
        }*/
        if(Storage::disk('state')->has('countries.json')){
            return json_decode(Storage::disk('state')->get('countries.json'));
        }else{
            return [];
        }

    }

    /**
     * @param $str
     * @return string
     */
    public static function generateSlug($str)
    {
        return strtolower(preg_replace('/[^a-zA-Z0-9]+/','_', $str));
    }

    /**
     * @param $code
     * @param $status
     * @param array $args
     * @return array
     */
    public static function AJAXResponse($code, $status, $args = [])
    {
        return array_merge([
            'code'      => $code,
            'message'   => $status
        ], $args);
    }

    public static function maskData($str)
    {
        $arr = str_split($str);
        $newArr = [];

        if(count($arr) > 5){
            foreach($arr as $i => $item){
                if ($i == 0)
                    $newArr[] = $item;
                elseif ($i >= count($arr)-2){
                    $newArr[] = $item;
                }else{
                    $newArr[] = '*';
                }
            }
        }else{
            foreach($arr as $i => $item){
                    $newArr[] = '*';
            }
        }
        return implode($newArr,'');

    }

}