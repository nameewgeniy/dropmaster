<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Front\MailController;
use App\Models\Order;
use App\Models\Sale;
use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrdersController extends AdminController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $orders = Order::orderBy('created_at', 'desc')->get();

        return view('admin.orders.orders', ['orders' => $orders]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $order = Order::find($id);

        /* Удаляем заказ */
        $order->delete();
        $order->sales()->delete();

        return redirect()->back();
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'info.*.track'    => 'required',
            'info.*.details'  => 'required',
            'status'          => 'required'
        ]);

        if(!$validator->fails()){

            // обнавляем статус у заказа
            $order = Order::find($id)
                ->update([ 'status' => $request['status'] ]);

            // обновляем информацию у продаж
            foreach( $request['info'] as $id_sale => $sale){

                Sale::whereId($id_sale)
                    ->update([
                        'track'   => $sale['track'],
                        'details' => $sale['details']
                    ]);

            }

            MailController::sendUpdateOrderInfoToClient($id);

        }

        return redirect()->back();
    }
}
