<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;

/**
 * Class CategoryController
 * @package App\Http\Controllers\Admin
 */
class CategoryController extends AdminController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //Category::getChildren();

        return view('admin.categories.categories', [
            'categories' => Category::all()
        ]);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function newCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|max:255',
            'parent_id' => 'required|string|max:255'
        ]);

        if(!$validator->fails())
        {
            $args = [
                'name'      => $request['name'],
                'slug'      => $request['slug']
            ];

            if($request['parent_id'] != 0)
            {
                $args['parent_id'] = $request['parent_id'];
            }

            $category = Category::create($args);
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $category = Category::find($id);

        /* Удаляем категорию */
        $category->delete();

        return redirect()->back();
    }
}
