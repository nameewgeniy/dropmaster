<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\Storage;

/**
 * Class ImagesController
 * @package App\Http\Controllers\Admin
 */
class ImagesController extends AdminController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $Images = Image::orderBy('created_at', 'desc')->paginate(18);

        if ($request->ajax())
        {
            echo view('admin.images.modal', [
                'images' =>  $Images
            ]);
            exit;
        }else{
            return view('admin.images.images', [
                'images' =>  $Images
            ]);
        }
    }

    /**
     * @param Request $request
     * @return null
     */
    public function newImage(Request $request)
    {
        if($request->isMethod('post'))
        {
            if($request->hasFile('files'))
            {
                $files = $request->file('files');
                foreach($files as $file)
                {
                    $newName = uniqid('img_', true). '.jpg';

                    if($file->move(config('filesystems.disks.images.root'), $newName))
                    {
                        Image::generateImagesSize(config('filesystems.disks.images.root') . '/' . $newName );

                        $images = new Image();
                        $images->src = $newName;
                        $images->save();
                    }
                }

            }
        }
        return null;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAll()
    {
        /* Удаление физических файлов  */
        $files = Storage::disk('images')->allFiles();
        Storage::disk('images')->delete($files);

        /* Удаление файлов из базы данных  */
        Image::truncate();

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $images = Image::find($id);
        if(Storage::disk('images')->delete($images->src))
        {
            $images->delete();
            Storage::disk('images')->delete($images->src.'.220x220.jpg');
            Storage::disk('images')->delete($images->src.'.500x500.jpg');
            Storage::disk('images')->delete($images->src.'.50x50.jpg');
        }
        return redirect()->back();
    }



}
