<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

/**
 * Class PostController
 * @package App\Http\Controllers\Admin
 */
class PostController extends AdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.post.posts', [ 'posts' => Post::all() ]);
    }

    /**
     * @param null $id
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newPost( $id = null, Request $request )
    {
        $data = [
            'categories' => Category::all()
        ];

        if(!is_null($id))
        {
            $data['post'] = Post::find($id);
        }

        if($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(), [
                'title'         => 'required|string',
                'h1'            => 'required|string',
                'content'       => 'required|string',
                'keywords'      => 'required|string',
                'description'   => 'required|string',
                'categories'    => 'array',
                'status'        => 'required'
            ],[
                'content.string' => 'The post content field is required.'
            ]);

            if(!$validator->fails())
            {
                $newPost = Post::updateOrCreate(['id' => $id ],[
                    'title'         => $request['title'],
                    'text'          => $request['content'],
                    'thumbnail_id'  => $request['thumbnail'],
                    'keywords'      => $request['keywords'],
                    'description'   => $request['description'],
                    'h1'            => $request['h1'],
                    'status'        => $request['status'],
                    'user_name'     => Auth::user()->name,
                    'user_id'       => Auth::user()->id,
                ]);

                /* Отвязываем все категории */
                $newPost->categories()->detach();

                /* Привязываем нужные категории */
                if(isset($request['categories']))
                {
                    foreach($request['categories'] as $catID)
                    {
                        $newPost->categories()->attach($catID);
                    }
                }

                $data['post'] = $newPost;
            }else{
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
        return view('admin.post.new', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $post = Post::find($id);

        /* Отвязываем все категории */
        $post->categories()->detach();

        /* Удаляем пост */
        $post->delete();

        return redirect()->back();
    }
}
