<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Cookie\CookieJar;

class AdminController extends Controller
{
    public function __construct()
    {
        Cookie::queue('dropStore', 'true', 2628000);
        $this->middleware('auth');
    }


}
