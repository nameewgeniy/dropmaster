<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProductController
 * @package App\Http\Controllers\Admin
 */
class ProductController extends AdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.product.products');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $product = Product::find($id);

        /* Отвязываем все категории */
        $product->categories()->detach();

        /* Отвязываем все изображения */
        $product->images()->detach();

        $product->options()->delete();
        $product->variations()->delete();
        $product->comments()->delete();


        /* Удаляем пост */
        $product->delete();

        return redirect()->back();
    }

    /**
     * @param null $id
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function newProduct($id = null, Request $request)
    {
        $data = [
            'categories' => Category::all(),
            'product'    => null
        ];

        if(!is_null($id))
        {
            $product = Product::find($id);

            $data['product'] = $product;
            $data['options'] = $product->renderOptions($product->options()->get()->toArray());
            $data['default'] = $product->getVariations('default');
            $data['variations'] = $product->getVariations();
        }

        if($request->isMethod('post'))
        {
            $validator = $this->validator($request->all());

            if(!$validator->fails())
            {
                $newProduct = Product::createOrUpdate(array_merge($request->all(), [ 'id' => $id ]));

                return redirect()->action('Admin\ProductController@newProduct', ['id' => $newProduct->id ]);

            }else{
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }

        return view('admin.product.new', $data);
    }



    /**
     * @param $request
     * @return mixed
     */
    protected function validator($request)
    {
        $validator = Validator::make($request, [

            'title'                 => 'required|string',
            'title_seo'             => 'string',
            'content'               => 'string',
            'categories'            => 'array',
            'images'                => 'required|array',
            'status'                => 'required',
            'slug'                  => 'required',

            'variations.*.price'    => 'required_with:variations',
            'variations.*.count'    => 'required_with:variations',
            'variations.*.sku'      => 'required_with:variations',
            'variations.*.name'     => 'required_with:variations',

            'default.price'         => 'required_without:variations',
            'default.count'         => 'required_without:variations',

            'options.*.name'        => 'required_with:options',
            'options.*.values'      => 'required_with:options',
        ]);

        return $validator;
    }
}
