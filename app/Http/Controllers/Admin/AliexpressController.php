<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Front\HomeController;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Helpers\Functions;

/**
 * Class ProductController
 * @package App\Http\Controllers\Admin
 */
class AliexpressController extends HomeController
{


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(Request $request)
    {
        if (Auth::check())
        {
            $image = new Image();
            $validator = $this->validator($request->all());

            if($validator->fails())
            {
                return response()->json(Functions::AJAXResponse(401, 'No validation data'));
            }else{

                $product = Product::createOrUpdate(array_merge( $this->checkVariations($request), [
                    'id'            => null,
                    'title'         => $request['title'],
                    'link'          => isset($request['link']) ? $request['link'] : null,
                    'content'       => $this->renderSpecifics($request),
                    'keywords'      => '',
                    'description'   => '',
                    'title_seo'     => $request['title'],
                    'slug'          => $request['title'],
                    'status'        => 1,
                    'images'        => $image->uploadFromURL($request['images']),
                    'categories'    => Category::createCategoryByName($request['category']),
                    'options'       => isset($request['options']) ? $request['options'] : null,
                    'comments'      => $request['comments']
                ]));
                if($product->id){
                    return response()->json(Functions::AJAXResponse(200, 'Product is added', ['productId' => $product->id ]));
                }else{
                    return response()->json(Functions::AJAXResponse(402, 'Product is not added'));
                }
            }
        }else{
            return response()->json(Functions::AJAXResponse(400, 'You is not login'));
        }

    }

    /**
     * @param $request
     * @return string
     */
    protected function renderSpecifics($request)
    {
        $specifics = '';

        if(isset($request['specifics']))
        {
            $specifics = '<ul>';
            foreach($request['specifics'] as $item )
            {
                $specifics .= '<li><strong>' . $item['name'] . '</strong> ' . $item['value'] . '</li>';
            }
            $specifics .= '</ul>';


        }
        return $specifics;
    }

    /**
     * @param $request
     * @return mixed
     */
    protected function validator($request)
    {
        $validator = Validator::make($request, [
            'title'         => 'required|string',
            'variations'    => 'required'
        ]);

        return $validator;
    }

    protected function checkVariations($request)
    {
        // Если всего одна вариация и SKU пустой
        // то это записываем как дефолтное значение
        // для товара
        $variations = [];
        if (isset($request['variations'])
            && count($request['variations']) == 1
            && $request['variations'][0]['sku'] == '')
        {
            $variations['default'] = $request['variations'][0];
        }else{
            $variations['variations'] = $request['variations'];
        }

        return $variations;
    }
}
