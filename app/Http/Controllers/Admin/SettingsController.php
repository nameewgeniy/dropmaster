<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\Facades\Validator;

class SettingsController extends AdminController
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')){

            if (isset($request['settings']) && !empty($request['settings'])){
                foreach($request['settings'] as $name => $value)
                {
                    if(!is_null($value) && !empty($value)){
                        Setting::updateOrCreate(['name' => $name ], [
                            'name'  => $name,
                            'value' => $value
                        ]);
                    }
                }
            }
        }
        return view('admin.settings.settings', [ 'settings' => Setting::all()->toArray() ]);
    }
}
