<?php

namespace App\Http\Controllers\Admin;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class PostController
 * @package App\Http\Controllers\Admin
 */
class CouponController extends AdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.coupon.coupons', [ 'coupons' => Coupon::all() ]);
    }

    /**
     * @param null $id
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|null
     */
    public function add( $id = null, Request $request )
    {
        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'name'          => 'required|string',
                'code'          => 'required|string',
                'value'         => 'required|string'
            ]);

            if (!$validator->fails()) {

                Coupon::updateOrCreate(['id' => $id ],[
                    'name'          => $request['name'],
                    'code'          => $request['code'],
                    'value'         => $request['value'],
                    'end_date'      => isset($request['end_date'])? $request['end_date'] : null
                ]);

                return redirect()->route('coupons');

            }else{
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
        return null;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $post = Coupon::find($id)->delete();
        return redirect()->back();
    }
}
