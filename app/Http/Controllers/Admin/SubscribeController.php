<?php namespace App\Http\Controllers\Admin;


use App\Models\Subscribe;

class SubscribeController extends AdminController {


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.subscribe.subscribes', [ 'subscribes' => Subscribe::all() ]);
    }

}
