<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN', 'dropmaster.org'),
        'secret' => env('MAILGUN_SECRET','key-cce641d1dad73519c5ef5c68eda866d8'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'paypal' => [
        'client_id' => env('PAYPAL_CLIENT_ID'),
        'secret'    => env('PAYPAL_CLIENT_SECRET'),
        'mode'      => env('PAYPAL_MODE'),
        //'client_id' => 'AVR9m9mTOdpocwf-IjJ5ml-fKrfU_5wloG3nNivJnfw61AnHkKRB0zCriZ0F3uWV2VrB3eY1pzvDaaGR',
        //'client_id' => 'AWFtkB2A1oCIOj9xgskJzk7u-Uvea1sTU2AEOdW8k8FfxS9vd5lqUwqOVTHgcVz8vA3MspA4ah1k2mEj', // live
        //'secret'    => 'EMrzVTd1kwER_jdY4pJPebvh_XS8SpTy_FdBfjoVL16962ZtiX4uCvz5vxvArIbfLDKg8z8eu_KwO3Kl', // live
        //'secret'    => 'EK5QF1O9AX9WbegCrpe0d9arUp061vZf-K5VsOrQB4cxHmBBaCZSbfEJKpREuv_i3_3kGes_UWeilK-W',
        'currency'  => 'USD'
    ],

    'template' => [
        'path' => 'v1',
        'name' => 'Template v1'
    ],


];
