//###############################################
//  INIT FUNCTIONS
//###############################################
jQuery(document).ready(function ($) {

    var args = {
        page: 2
    };
    /* Add images to Product */
    /*$('#add-images-product').on('click', function(){

    });

    var checkedImages = [];
    jQuery(selectorModal).find('.add-image').on('click', function(){
        checkedImages = jQuery(this).find('label[aria-checked="true"]');
    });

    console.log(checkedImages);*/
    /* Add Images to Post and Product*/
    $('#add-image').on('click', function(){

        var checkedImages = $(this).parents('#media').find('label[aria-checked="true"]');
        var images  =  $('#images');
        var thumbnail = $('#thumbnail');

        if(thumbnail.length > 0)
        {
            var thumbnailName = $('input[name="thumbnail"]');
            thumbnail.html('<img src="'+checkedImages.data('src')+'" />');
            thumbnailName.val(checkedImages.data('value'));
        }

        if(images.length > 0)
        {
            checkedImages.each(function(i,item){
                var html = '<div class="uk-thumbnail uk-margin-bottom uk-width-medium-1-5 img-product">' +
                    '<img src="' + $(item).data('src') + '" />' +
                    '<div class="uk-thumbnail-caption">' +
                    '<input type="hidden" name="images[]" value="' + $(item).data('value') + '"/>' +
                    '<a onclick="deleteThisItem(this, \'.img-product\')" class="uk-button uk-button-danger delete-img">' +
                    '<i class="uk-icon-trash-o"></i></a></div>' +
                    '</div>';
                images.append(html);
            });
        }
    });

    $('body').on('click','.more-images', function(){

        var modal = $(this).parents('.media-modal');
        Images.loadImages(args.page, modal);
        args.page++;

    });



});

var Images = {

    init: function(el, type){

        var htmlModal = [
            '<div class="uk-modal media-modal" >',
            '<div class="uk-modal-dialog uk-modal-dialog-blank" style="padding: 15px" >',
            '<a class="uk-modal-close uk-close"></a>',
            '<h2>Images</h2>',
            '<div class="uk-overflow-container">',
            '<div class="uk-grid uk-grid-width-1-10 uk-margin-remove uk-grid-small" style="padding: 15px" data-uk-button-checkbox>',
            '</div>',
            '<div class="uk-modal-spinner"></div>',
            '<p class="uk-icon-arrow-circle-down more-images"></p>',
            '</div>',
            '<div class="uk-modal-footer uk-clearfix">',
            '<button class="uk-float-right uk-button uk-button-primary add-image uk-modal-close" onclick="Images.clickSetImage(this,\'' + type + '\')" type="button">Set image</button>',
            '</div>',
            '</div>',
            '</div>'
        ].join("");

        jQuery(el).after(htmlModal);

        var selectorModal = jQuery(el).next('.media-modal');
        UIkit.modal(selectorModal).show();

        this.loadImages(1,selectorModal);

    },

    deleteModal: function(){
        jQuery('.media-modal').remove();
        jQuery('html').removeClass('uk-modal-page');
    },

    loadImages: function(page, modal){

        jQuery(modal).find('.uk-modal-spinner').css('display','block');

        jQuery.ajax({
            method: "GET",
            url: '/admin/images?page=' + page
        }).done(function( data ) {

            jQuery(modal).find('.uk-modal-spinner').css('display','none');
            jQuery(modal).find('.uk-overflow-container .uk-grid').append(data);

        }).fail(function( err ){
            console.log(err);
        });

    },

    clickSetImage: function(el,type){

        switch (type) {

            case 'product':
                this.setImageProduct(el);
                this.deleteModal();
                break;

            case 'option':
                this.setImageOption(el);
                this.deleteModal();
                break;

            case 'post':
                this.setImagePost(el);
                this.deleteModal();
                break;

            default:
                this.deleteModal();
                break;
        }
    },

    setImageProduct: function(el){

        var imagesBox = jQuery('#images');
        var checkedImages = jQuery(el).parents('.media-modal').find('label[aria-checked="true"]');

        if(checkedImages.length > 0 && imagesBox.length > 0){
            checkedImages.each(function(i,item){
                var html = '<div class="uk-thumbnail uk-margin-bottom uk-width-medium-1-5 img-product">' +
                    '<img src="' + jQuery(item).data('src') + '" />' +
                    '<div class="uk-thumbnail-caption">' +
                    '<input type="hidden" name="images[]" value="' + jQuery(item).data('value') + '"/>' +
                    '<a onclick="deleteThisItem(this, \'.img-product\')" class="uk-button uk-button-danger delete-img">' +
                    '<i class="uk-icon-trash-o"></i></a></div>' +
                    '</div>';
                imagesBox.append(html);
            });
        }
    },

    setImagePost: function(el){

        var thumbnail = jQuery('#thumbnail');
        var thumbnailName = jQuery('input[name="thumbnail"]');
        var checkedImages = jQuery(el).parents('.media-modal').find('label[aria-checked="true"]');

        if( thumbnail.length > 0 && checkedImages.length > 0 && thumbnailName.length > 0){
            thumbnail.html('<img src="'+checkedImages.data('src')+'" />');
            thumbnailName.val(checkedImages.data('value'));
        }
    },

    setImageOption: function(el){

        var checkedImages = jQuery(el).parents('.media-modal').find('label[aria-checked="true"]');
        var box = jQuery(el).parents('td').find('.var-img');
        var input = jQuery(el).parents('td').find('.var-img').find('input');

        if( box.length > 0 && input.length > 0 && checkedImages.length > 0){

            box.css('background-image', 'url("' + checkedImages.data('src').replace(/220x220/gi, '50x50') + '")');
            input.val(checkedImages.data('value'));

        }

    }
};
//###############################################
//  INIT FUNCTIONS END
//###############################################



//#############################################
//## UPLOAD IMAGES
//#############################################

jQuery(function(){

    if(jQuery("#progressbar").length > 0 ){

        var progressbar = jQuery("#progressbar"),
            bar         = progressbar.find('.uk-progress-bar'),
            CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content'),
            settings    = {

                action: '/admin/images/new', // upload url
                method: 'POST',
                params: {_token: CSRF_TOKEN},
                allow : '*.(jpg|jpeg|gif|png)', // allow only images

                loadstart: function() {
                    bar.css("width", "0%").text("0%");
                    progressbar.removeClass("uk-hidden");
                },

                progress: function(percent) {
                    percent = Math.ceil(percent);
                    bar.css("width", percent+"%").text(percent+"%");
                },

                allcomplete: function(response) {

                    bar.css("width", "100%").text("100%");

                    setTimeout(function(){
                        progressbar.addClass("uk-hidden");
                    }, 250);

                    location.reload()

                }
            };
        var select = UIkit.uploadSelect(jQuery("#upload-select"), settings),
            drop   = UIkit.uploadDrop(jQuery("#upload-drop"), settings);
    }
});

//###############################################
//  UPLOAD IMAGES END
//###############################################




//###############################################
//  GENERATE SLUG
//###############################################

function liveGenerateSlug(el){
    var Text = el.value;
    Text = Text.toLowerCase();
    Text = Text.replace(/[^a-zA-Z0-9]+/g,'_');
    document.querySelector('input[name="slug"]').value = Text;
}

//###############################################
//  GENERATE SLUG END
//###############################################

/* Delete this item */
function deleteThisItem(e, item, callback){

    var q = confirm('Delete this item?');

    if(q){

        jQuery(e).parents(item).remove();

        if (callback && typeof(callback) === "function") {
            callback();
        }
    }
}

//###############################################
//  OPTIONS
//###############################################

var Options = {

    addOption: function(){
        var count = jQuery('.options-table .one-option').length + 1;
        var html = [
            '<tr class="one-option">',
                '<td>',
                    '<label for="">Option name</label>',
                    '<input type="text" placeholder="Name" name="options[' + count + '][name]" class="uk-width-1-1">',
                '</td>',
                '<td>',
                    '<label for="">Option values</label>',
                    '<textarea onchange="Options.genVariations(event)" class="uk-width-1-1" name="options[' + count + '][values]" placeholder="Separate options with a comma" id="" cols="40" rows="5" >',
                    '</textarea>',
                '</td>',
                '<td>',
                    '<a class="uk-button-danger uk-button" onclick="deleteThisItem(this, \'tr\', function(){ Options.genVariations(event) })" style="margin-top: 20px">',
                        '<i class="uk-icon-trash-o" ></i>',
                    '</a>',
                '</td>',
            '</tr>'
        ].join("");
        jQuery('.options-table tbody').prepend(html);
    },

    genVariations: function(event){

        //if(/*event.keyCode == 188 || */event.type == 'blur'){

        var attributes = jQuery('body .one-option textarea');
        var option  = jQuery('body .one-option input');
        var arrAttributes = [];

        attributes.each(function(i,e){
            var arrOne = jQuery(e).val().split(',');
            arrAttributes.push(arrOne)
        });

        this.render( arrAttributes );
        //}
    },

    /* рендерим вариации в таблицу  */
    render: function (arrAttributes){

        if(arrAttributes.length == 0){
            jQuery('.variations-table').css('display', 'none').find('tbody').html('');
            return false;
        }
        var result = [];
        var oldData = {
            images: jQuery('.variations-table  [name *="[img]"]').map(function(i, el){ return jQuery(el).val(); }),
            srcImg: jQuery('.variations-table  .var-img').map(function(i, el){ return jQuery(el).attr('style'); }),
            count : jQuery('.variations-table  [name *="[count]"]').map(function(i, el){ return jQuery(el).val(); }),
            price : jQuery('.variations-table  [name *="[price]"]').map(function(i, el){ return jQuery(el).val(); })
        };

        this.combinator(arrAttributes).forEach(function(item, i){

            var variant = Array.isArray(item)? item.join(' &#9679; ') : item;
            var sku = Array.isArray(item)? item.join('-') : item;

            result.push([
                '<tr>',
                    '<td>',
                        '<input type="checkbox" checked name="variations['+ i +'][check]"/>',
                        '<input type="hidden" value="' + variant  + '" name="variations[' + i + '][name]" />',
                    '</td>',
                    '<td>' + variant  + '</td>',
                    '<td><div class="def-img var-img" onclick="Images.init(this,\'option\')" style=\'' + defaultIfUndefined(oldData.srcImg[i],'') + '\'>',
                        '<input type="hidden" value="' + defaultIfUndefined(oldData.images[i], '') + '" name="variations[' + i + '][img]" />',
                    '</div></td>',
                    '<td><input type="text" readonly  value="' + sku.toLowerCase()  + '" name="variations['+ i +'][sku]"/></td>',
                    '<td><input type="text"  value="' + defaultIfUndefined(oldData.count[i], '') + '" name="variations['+ i +'][count]"/></td>',
                    '<td><input type="text" data-price="' + defaultIfUndefined(oldData.price[i], '') + '" class="price-option" value="' + defaultIfUndefined(oldData.price[i], '') + '" placeholder="$0.00" name="variations['+ i +'][price]" /></td>',
                    '<td></td>',
                '</tr>'
            ].join(""));

        });
        jQuery('.variations-table').css("display", "table").find('tbody').html(result.join(""));

        /**
         *
         * @param value
         * @param defValue
         */
        function defaultIfUndefined(value, defValue){
            if(value === undefined){
                return defValue;
            }else{
                return value;
            }
        }

        return false;
    },

    /* все возможные вариации двумерного массива, крутая функция, спасбио Кириллу */
    combinator: function (matrix){
        return matrix.reduceRight(function(combination, x){
            var result = [];
            x.forEach(function(a){
                combination.forEach(function(b){
                    result.push( [ a ].concat( b ) );
                });
            });
            return result;
        });
    },

    calc: function(){

        var $rate = jQuery('#rate').val();
        var $prices = jQuery('.price-option');

        if ($rate.length != ''){
            $prices.map( function(i, el){
                var price = jQuery(el).data('price') * $rate;
                jQuery(el).val( price.toFixed(2) );
            });
        }

    }
};

//###############################################
//  OPTIONS END
//###############################################

