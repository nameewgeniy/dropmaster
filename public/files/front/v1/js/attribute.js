jQuery(document).ready(function ($)
{
    /* actions */
    $('body')
        .on('change','div[data-uk-button-radio]', function()
        {
            Attributes.infoStock();
        })
        .on('change', '#quantity', function()
        {
            if($(this).val() != 0){
                Attributes.infoStock();
            }
        })
});

var Attributes = {

    settings: {
        quantity: '#quantity',
        pieces  : '#product .pieces',
        boxOptions: '.options',
        product: '#product'

    },

    getCountItems: function(){
        return parseInt(jQuery(this.settings.quantity).val());
    },

    setPieces: function(count){
        jQuery(this.settings.quantity).attr('data-max',count);
        jQuery(this.settings.pieces).html(count + ' pieces available');
        return this;
    },

    getSKU: function(){
        var attr = [];
        var input = jQuery(this.settings.boxOptions + ' input[type="radio"]:checked');

        if(input.length > 0){
            input.each( function(i, e){
                attr.push(jQuery(e).val().toLowerCase());
            } );
            return attr.join('-');
        }else{
            return 'default';
        }

    },

    setPrice: function(price){
        if(!isNaN(price)){
            price = parseFloat(price) * this.getCountItems();
            jQuery('.total-price-value').html( price.toFixed(2) + ' ' + Product.currency);
        }else{
            jQuery('.total-price-value').html( price );
        }
        return this;
    },

    checkAttr: function(){

        var error = [];
        var attrInfo = jQuery(this.settings.boxOptions);

        if(attrInfo.length != 0){

            attrInfo.find('div[data-uk-button-radio]').each(function(i, e){

                var radio = jQuery(e).find('input[type="radio"]:checked');
                var messageBox = jQuery(this).find('.uk-alert-warning');

                if( radio.length == 0 ){
                    if( messageBox.length > 0 && messageBox.hasClass('d-none') ){
                        messageBox.removeClass('d-none');
                    }
                    error.push(i);

                }else if( radio.length > 0 ){

                    if( messageBox.length > 0 && !messageBox.hasClass('d-none') ){
                        messageBox.addClass('d-none');
                    }
                }
            });
            return error.length;
        }else{
            return 0;
        }
    },

    infoStock: function(){

        if( this.checkAttr() == 0 ){

            // Деактивируем кнопку до звгрузки информации
            this.disableAddToCartButton();

            var data = {
                productId   : Product.id,
                sku         : this.getSKU()
            };
            this.setPrice('<i class="uk-icon-refresh uk-icon-spin" ></i>');

            $.ajax({
                url: '/info/stock',
                method: 'POST',
                data: data,
                success:function(response){

                    if( response.id != null ){

                        Attributes.enableAddToCartButton()
                                  .setPieces(response.count)
                                  .setPrice( response.price );
                        // Устанавливаем выбранную вариацию как текущую
                        // для добавления в корзину
                        Product.current = response.id;

                    // Вставлять картинку в галерею response.img( это будет id )
                        if(response.src != null){

                            var productSlider = $( '.fotorama' ).data( 'fotorama' );
                            var index = false;

                            jQuery.each( productSlider.data, function ( i, e ) {
                                if ( e.img == response.src.replace('220x220', '500x500') ) index = i;
                            } );

                            if(!index){
                                index = productSlider.push( { img : response.src.replace('220x220', '500x500'), thumb : response.src} ).size;
                                productSlider.show(index-1);
                            }else{
                                productSlider.show(index);
                            }
                        }

                    }else{
                        console.log(response);
                    }
                },
                error:function(err){
                    console.log(err);
                }
            });
        }
    },

    currentSKU: function(){
        return {
            id: jQuery(this.settings.product).attr('data-current'),
            count: this.getCountItems()
        }
    },

    disableAddToCartButton: function(){
        jQuery('.add-to-cart').attr('disabled', 'disabled');
    },

    enableAddToCartButton: function(){
        jQuery('.add-to-cart').removeAttr('disabled');
        return this;
    }

};

