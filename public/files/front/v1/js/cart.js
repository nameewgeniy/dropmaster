/**
 * Created by nakap on 16.08.17.
 */
jQuery(document).ready(function ($){

    $.cookie.json = true;

    $('body')

        .on('click', '.add-to-cart', function(){

        if( Attributes.checkAttr() == 0 ){

            var modal = UIkit.modal(jQuery('#modal-cart'));

            Cart.add();
            modal.show();

        }else{
            UIkit.notify('Please select options',{status:'danger', timeout : 1500});
        }
    });

});

var Cart = {

    settings: {
        items: '.header-cart-count'
    },

    refresh: function(){

        var $items = this.items();
        var $count = 0;

        for (var id in $items) {
            $count += $items[id];
        }

        jQuery.cookie('count', $count,{ path: '/', expires: 1});
        jQuery(this.settings.items).html($count);

    },

    add: function(){

        var $items = this.items();
        $items[Product.current] =  Attributes.getCountItems();

        jQuery.cookie('cart', $items,{ path: '/', expires: 1});

        this.refresh();
        return this;
    },

    clear: function(){

        jQuery.cookie('cart', null,{ path: '/', expires: 1});
        jQuery.cookie('count', 0,{ path: '/', expires: 1});
        return true;
    },

    remove: function(id, el){

        var $items = this.items();

        if (id in $items){

            delete $items[id];
            jQuery.cookie('cart', $items,{ path: '/', expires: 1});

        }

        jQuery(el).parents('tr').remove();
        this.refresh();
        location.reload()

    },

    items: function(){

        if(jQuery.cookie('cart') !== undefined && jQuery.cookie('cart') != null){
            return jQuery.cookie('cart');
        }else{
            return {};
        }
    },

    disableButton: function(el){
        el.attr('disabled', 'disabled');
    },

    setStateByCountryCode: function(code){

        $.ajax({
            url: '/info/state',
            method: 'GET',
            data: { code: code },

            success:function(response){
                console.log(response);
                if(response != null && response.length > 0){

                    var html = '<select name="client[region]" required class="uk-width-1-1" >';

                        response.forEach(function($val, $index){
                            html += '<option value="' + $val.name +' >' + $val.name +'</option>'
                        });
                    html += '</select>';
                    $('#region').html(html);

                }else{
                    $('#region').html('<input type="text" required class="uk-width-1-1"  name="client[region]">');
                }


            },
            error:function(err){
                console.log(err);
            }
        });

    },

    changeCountry: function(el){

        var $code = $(el).val();
        Cart.setStateByCountryCode($code);

    },

    setPromocode: function(el){
        var promo = $('[name="promocode"]').val();
        var ref = window.location.href.replace( window.location.search, '');
        window.location.href = ref + '?promocode=' + promo;
    }
};
