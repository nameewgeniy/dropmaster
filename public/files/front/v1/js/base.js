/**
 * Created by nakap on 31.07.17.
 */
jQuery(document).ready(function ($) {

/* Quantity count  */
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        var $max   = $(this).parent().find('input').data('max');
        if( parseInt($input.val()) + 1 > $max ){
            return false;
        }
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

    /* Refresh cart count */
    $.cookie.json = true;
    if(jQuery.cookie('count') !== undefined ){
        jQuery('.header-cart-count').html(jQuery.cookie('count'));
    }

    console.log(Home.loadListCategories());


});

var Home = {

    isHome: jQuery('#best-seller').length,

    loadCategory: function($slug_category){

    },

    loadListCategories: function(){
    }

};

