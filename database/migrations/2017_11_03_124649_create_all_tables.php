<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('categories')){
            Schema::create('categories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug');
                $table->integer('parent_id')->nullable();
                $table->integer('lft')->nullable();
                $table->integer('rgt')->nullable();
                $table->integer('depth')->nullable();
                $table->timestamps();
            });

            Schema::create('category_post', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('category_id');
                $table->integer('post_id');
            });

            Schema::create('category_product', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('category_id');
                $table->integer('product_id');
            });

            Schema::create('image_product', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('image_id');
                $table->integer('product_id');
            });

            Schema::create('comments', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('country');
                $table->integer('stars');
                $table->integer('product_id');
                $table->text('comment')->nullable();
                $table->string('date');
                $table->json('images')->nullable();
                $table->timestamps();
            });

            Schema::create('images', function (Blueprint $table) {
                $table->increments('id');
                $table->string('src');
                $table->timestamps();
            });

            Schema::create('options', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->nullable();
                $table->integer('img')->nullable();
                $table->string('name');
                $table->string('value');
                $table->timestamps();
            });

            Schema::create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->string('status');
                $table->string('promocode')->nullable();
                $table->string('price');
                $table->string('key');
                $table->string('payer_id')->nullable();
                $table->string('payment_id')->nullable();
                $table->json('client');
                $table->timestamps();
                $table->softDeletes();
            });

            Schema::create('posts', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->text('text');
                $table->text('keywords');
                $table->text('description');
                $table->text('h1');
                $table->string('user_name');
                $table->string('user_id');
                $table->string('thumbnail_id')->nullable();
                $table->integer('status')->default(0);
                $table->timestamps();
            });

            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->text('title_seo');
                $table->text('text');
                $table->string('status');
                $table->text('description');
                $table->text('keywords');
                $table->string('user_name');
                $table->string('user_id');
                $table->string('slug');
                $table->string('link')->nullable();
                $table->string('rate')->nullable();
                $table->string('orders')->nullable();
                $table->timestamps();
            });

            Schema::create('sales', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('order_id');
                $table->integer('product_id');
                $table->string('name');
                $table->string('variation');
                $table->string('count');
                $table->string('price');
                $table->string('sku');
                $table->string('currency');
                $table->string('img')->nullable();
                $table->string('track')->nullable();
                $table->string('details')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

            Schema::create('settings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->text('value');
                $table->timestamps();
            });

            Schema::create('variations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id');
                $table->integer('img')->nullable();
                $table->string('sku');
                $table->string('price');
                $table->string('count');
                $table->string('check');
                $table->string('name');
                $table->string('sale')->nullable();
                $table->timestamps();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
