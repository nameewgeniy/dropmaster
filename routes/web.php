<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Frontend routes */
Route::get('/', 'Front\HomeController@index');
Route::get('product/{slug}', 'Front\ProductController@index');
Route::get('category/{slug}', 'Front\CategoryController@index');
Route::get('order/status', 'Front\PageController@order');
Route::get('track', 'Front\PageController@track')->name('track');
Route::get('search', 'Front\SearchController@index');
Route::get('subscribe', 'Front\SubscribeController@add')->name('subscribe');

Route::get('privacy-policy', 'Front\PageController@privacyPolicy');
Route::get('terms-and-conditions', 'Front\PageController@termsAndConditions');
Route::get('payment-methods', 'Front\PageController@paymentMethods');
Route::get('shipping-delivery', 'Front\PageController@shippingDelivery');
Route::get('returns-policy', 'Front\PageController@returnsPolicy');
Route::get('faq', 'Front\PageController@faq');
Route::get('contact-us', 'Front\PageController@contactUs');
Route::get('contact-us/send', 'Front\MailController@sendContactUs');
Route::get('success', 'Front\PageController@success');
Route::get('error', 'Front\PageController@error');
Route::get('feed/{slug}', 'Front\FeedController@feed');
Route::get('/sitemap.xml', 'Front\SitemapController@sitemapXML');
Route::get('/categories.xml', 'Front\SitemapController@categories');
Route::get('/products.xml', 'Front\SitemapController@products');


Route::any('cart', 'Front\CartController@index');
Route::any('pay', 'Front\CartController@pay');
Route::get('callback', 'Front\CartController@callback');


Route::get('regions/{id}', 'Front\CartController@regions');

Route::post('info/stock', 'Front\ProductController@stock');
Route::any('info/state', 'Front\CartController@state');


/* Admin routes */
Route::group(['prefix' => 'admin'], function()
{
    Route::get('/', 'Admin\DashboardController@index');
    Route::any('settings', 'Admin\SettingsController@index');

    Route::group(['prefix' => 'orders'], function()
    {
        Route::get('/', 'Admin\OrdersController@index');
        Route::get('/delete/{id}', 'Admin\OrdersController@delete');
        Route::get('/update/{id}', 'Admin\OrdersController@update');
    });


    Route::group(['prefix' => 'images'], function()
    {
        Route::get('/', 'Admin\ImagesController@index');
        Route::post('/new', 'Admin\ImagesController@newImage');
        Route::get('/delete/all', 'Admin\ImagesController@deleteAll');
        Route::get('/delete/{id}', 'Admin\ImagesController@delete');
    });

    Route::group(['prefix' => 'categories'], function()
    {
        Route::get('/', 'Admin\CategoryController@index');
        Route::post('/new', 'Admin\CategoryController@newCategory');
        Route::get('/delete/all', 'Admin\CategoryController@deleteAll');
        Route::get('/delete/{id}', 'Admin\CategoryController@delete');
    });

    Route::group(['prefix' => 'coupon'], function()
    {
        Route::get('/', 'Admin\CouponController@index')->name('coupons');
        Route::post('/new', 'Admin\CouponController@add')->name('new_coupon');
        Route::get('/delete/all', 'Admin\CouponController@deleteAll')->name('coupon_delete_all');
        Route::get('/delete/{id}', 'Admin\CouponController@delete')->name('delete_coupon');
    });

    Route::group(['prefix' => 'subscribes'], function()
    {
        Route::get('/', 'Admin\SubscribeController@index')->name('subscribes');
    });

    Route::group(['prefix' => 'post'], function()
    {
        Route::get('/', 'Admin\PostController@index');
        Route::any('/new/{id?}', 'Admin\PostController@newPost');
        Route::get('/delete/{id?}', 'Admin\PostController@delete');
    });

    Route::group(['prefix' => 'product'], function()
    {
        Route::get('/', 'Admin\ProductController@index');
        Route::any('/new/{id?}', 'Admin\ProductController@newProduct');
        Route::any('/delete/{id?}', 'Admin\ProductController@delete');
        Route::post('/import', 'Admin\AliexpressController@import');
    });
});

// Authentication Routes...
Route::get('login', 'Admin\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Admin\Auth\LoginController@login');
Route::post('logout', 'Admin\Auth\LoginController@logout')->name('logout');

// Registration Routes...
/*Route::get('register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Admin\Auth\RegisterController@register');*/
Route::get('register', function(){
    abort(404);
})->name('register');
Route::post('register', function(){
    abort(404);
});

// Password Reset Routes...
Route::get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset');