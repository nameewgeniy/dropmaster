<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon shortcut" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>DropMaster Store</title>

    <!-- Styles -->
    <link href="{{ asset('files/admin/css/app.css') }}" rel="stylesheet">


    
</head>
<body >
    @if (!Auth::guest())
        <section class="registration" >
            <nav class="uk-navbar">
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
                <div class="uk-navbar-content uk-navbar-center">Admin Panel | <a href="{{ url('/') }}">Move to site</a></div>
            </nav>
        </section>
    @endif

    @yield('content')

    <!-- Scripts -->
    <script src="{{ asset('files/js/jquery.min.js') }}"></script>
    <script src="{{ asset('files/js/uikit.min.js') }}"></script>
    <script src="{{ asset('files/js/components/accordion.min.js') }}"></script>
    <script src="{{ asset('files/admin/js/base.js') }}"></script>
</body>
</html>
