@extends('admin.index')
@section('main')
<h3>Subscribes List</h3>
    <table class="uk-table">
        <thead>
        <tr>
            <th>Email</th>
            <th>Status</th>
            <th>Date</th>
            <!--<th></th>-->
        </tr>
        </thead>
        <tbody>
        @forelse($subscribes as $subscribe)
        <tr>
            <td>{{ $subscribe->email }}</td>
            <td>{{ $subscribe->subscribe }}</td>
            <td>{{ $subscribe->created_at->format('d-m-Y') }}</td>
            <!--<td>
                <a href="route('delete_coupon',[ $coupon->id ]) " class="uk-button-danger uk-button">
                    <i class="uk-icon-trash-o" ></i>
                </a>
            </td>-->
        </tr>
        @empty
            <p>Emails not found</p>
        @endforelse
        </tbody>
    </table>
@endsection
