@extends('admin.index')
@section('main')
<h3>Coupons</h3>
<form class="uk-form categories" action="{{ route('new_coupon') }}" method="post">
    {{ csrf_field() }}
    <table class="uk-table">
        <thead>
        <tr>
            <th>Coupon name</th>
            <th>Coupon code</th>
            <th>Coupon %/sale</th>
            <th>Coupon end date</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><input type="text" name="name" placeholder="name"></td>
            <td><input type="text" name="code" placeholder="code"></td>
            <td><input type="text" name="value" placeholder="%"></td>
            <td><input type="date" name="end_date" placeholder="end date"></td>
            <td><input type="submit" value="Add" class="uk-button-success uk-button"/></td>
        </tr>
        @forelse($coupons as $coupon)
            <tr>
                <td>{{ $coupon->name }}</td>
                <td>{{ $coupon->code }}</td>
                <td>{{ $coupon->value }}</td>
                <td>{{ $coupon->end_date }}</td>
                <td>
                    <a href="{{ route('delete_coupon',[ $coupon->id ]) }}" class="uk-button-danger uk-button">
                        <i class="uk-icon-trash-o" ></i>
                    </a>
                </td>
            </tr>
        @empty
            <p>Coupons not found</p>
        @endforelse
        </tbody>
    </table>
</form>

@endsection
