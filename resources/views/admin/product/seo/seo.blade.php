<div class="uk-panel uk-panel-box">
    <legend>SEO</legend>
    <p><label for="">Page title</label></p>
    <input
        type="text"
        placeholder="70 characters used"
        class="uk-width-1-1"
        name="title_seo"
        value="{{ $product->title_seo or '' }}"
        >
    <p><label for="">Meta description</label></p>
    <textarea style="height: 80px;" class="uk-width-1-1" placeholder="120 characters used" name="description"  >{!! $product->keywords or '' !!}</textarea>

    <p><label for="">Keywords</label></p>
    <textarea style="height: 80px;" class="uk-width-1-1" placeholder="keywords" name="keywords"  >{!! $product->description or '' !!}</textarea>

    <p><label for="">URL and handle</label></p>
    <input
        type="text"
        placeholder="{{ url('product') }}"
        class="uk-width-1-1 "
        name="slug"
        value="{{ $product->slug or '' }}"
        >
</div>