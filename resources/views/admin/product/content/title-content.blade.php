
<div class="uk-panel uk-panel-box">
    <p><label for="">Title</label></p>
    <input
        type="text"
        placeholder="Short Sleeve T-Shirt"
        class="uk-width-1-1 uk-form-large"
        name="title"
        value="{{ $product->title or '' }}"
        onchange="liveGenerateSlug(this)"
        >
    <p><label for="" >Post content</label></p>
    <textarea class="text uk-margin-top" style="height: 300px;" name="content" id="content" >{!! $product->text or '' !!}</textarea>
</div>