@isset($product->comments)
<legend>Comments</legend>
<ul class="uk-comment-list">
    @foreach($product->comments as $comment)
    <li>
        <article class="uk-comment">
            <header class="uk-comment-header" style="border: 0">
                <h4 class="uk-comment-title">
                    <strong>Name: </strong>
                    <input type="text" name="comments[{{ $loop->iteration }}][name]"  class="uk-width-1-1"   value="{{ $comment->name }}" />
                </h4>
            </header>
            <div class="uk-comment-body">
                <textarea name="comments[{{ $loop->iteration }}][comment]" class="uk-width-1-1" id="" cols="30" rows="5">{{ $comment->comment }}</textarea>
                <input type="hidden" name="comments[{{ $loop->iteration }}][country]" value="{{ $comment->country }}" />
                <input type="hidden" name="comments[{{ $loop->iteration }}][date]" value="{{ $comment->date }}" />
                <input type="hidden" name="comments[{{ $loop->iteration }}][stars]" value="{{ $comment->stars }}" />

                @if(!is_null( $comment->images ))
                    <div class="uk-flex uk-grid uk-grid-width-1-5" >
                        @foreach( json_decode($comment->images) as $image )
                            <div class="img-comment" >
                                <div class="uk-thumbnail uk-margin-top" style="background-image: url({{ $image }})">
                                </div>
                                <a class="uk-button-danger uk-button " onclick="deleteThisItem(this, '.img-comment')" style="margin-top: -163px">
                                    <i class="uk-icon-trash-o" ></i>
                                </a>
                                <input type="hidden" name="comments[{{ $loop->parent->iteration }}][images][]" value="{{ $image }}" />
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </article>
        <hr/>
    </li>
    @endforeach

</ul>
@endisset
