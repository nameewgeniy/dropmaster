<div class="uk-panel uk-panel-box">
    <div id="images" class="uk-grid" style="margin-left: 0" data-uk-grid-margin>
        @isset($product->images)
            @foreach($product->images as $image)
            <div class="uk-thumbnail uk-margin-bottom uk-width-medium-1-5 img-product uk-row-first">
                <img src="{{ App\Models\Image::getImageByID($image->id) }}">
                <div class="uk-thumbnail-caption"><input type="hidden" name="images[]" value="{{ $image->id }}">
                    <a onclick="deleteThisItem(this, '.img-product')" class="uk-button uk-button-danger delete-img">
                        <i class="uk-icon-trash-o"></i>
                    </a>
                </div>
            </div>
            @endforeach
        @endisset
    </div>
    <button class="uk-button" type="button" onclick="Images.init(this, 'product')" id="add-images-product" >Add images</button>
</div>