<div class="uk-panel uk-panel-box" id="options">
    <button class="uk-button" type="button" onclick="Options.addOption()" >Add option</button>
    <section class="panel-options">
        <table class="uk-table options-table uk-margin-bottom" >
            <tbody onchange="Options.genVariations(event)">
            @isset($options)
                @foreach($options as $name => $option)
                    <tr class="one-option">
                        <td>
                            <label for="">Option name</label>
                            <input type="text" placeholder="Name" name="options[{{ $loop->iteration }}][name]" value="{{ $name }}" class="uk-width-1-1">
                        </td>
                        <td>
                            <label for="">Option values</label>
                            <textarea
                                class="uk-width-1-1"
                                name="options[{{ $loop->iteration }}][values]"
                                placeholder="Separate options with a comma"
                                id=""
                                cols="40"
                                rows="5" >{{ implode(',', $option['values']) }}</textarea>
                        </td>
                        <td>
                            <a class="uk-button-danger uk-button" onclick="deleteThisItem(this, 'tr', function(){ Options.genVariations(event) })" style="margin-top: 20px">
                                <i class="uk-icon-trash-o" ></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endisset
            </tbody>
        </table>
        <!--  Variations -->
        @include('admin.product.variations.variations')
</section>
</div>