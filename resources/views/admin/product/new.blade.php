@extends('admin.index')
@section('main')
<script type="application/javascript" src="//cdn.tinymce.com/4/tinymce.min.js" ></script>
<h3>Add product</h3>
<form class="uk-form" id="new-product" action="" method="post">
    {{ csrf_field() }}
    <div class="uk-clearfix uk-margin">
        <button type="submit" class="uk-button uk-float-right uk-button-success"><i class="uk-icon-save "></i>  Save</button>
        <select name="status" class="uk-float-right uk-margin-right" >
            <option value="1" @isset($product->status) {{ Functions::selected(1, $product->status ) }} @endisset >Publish</option>
            <option value="0" @isset($product->status) {{ Functions::selected(0, $product->status ) }} @endisset >No publish</option>
        </select>
    </div>
    <div class="uk-grid">
        <div class="uk-width-4-6">

            <!--Title and content post-->
            @include('admin.product.content.title-content')


            <!--Images box-->
            @include('admin.product.images.images')

            <!--Pricing-->
            @include('admin.product.price.price')

            <!-- Options -->
            @include('admin.product.options.options')


            <!--SEO-->
            @include('admin.product.seo.seo')

            <div class="uk-panel uk-panel-box ">
                <!--COMMENTS-->

                @include('admin.product.comments.comments')
            </div>


        </div>
        <div class="uk-width-2-6">
            <!--Categories-->
            <div class="uk-panel uk-panel-box">
                <legend>Categories</legend>
                <div class="uk-scrollable-box" style="height:205px">
                    <ul class="uk-list">
                        @forelse($categories as $category)
                            <li>
                                <label>
                                    <input
                                        type="checkbox"
                                        name="categories[]"
                                        value="{{ $category->id }}"

                                        @isset($product->categories)
                                            @if(in_array( (string)$category->id, (array)$product->categories()->pluck('category_id')->toArray() ))
                                                checked="checked"
                                            @endif
                                        @endisset
                                            >
                                        {{ $category->name }}
                                </label>
                            </li>
                        @empty
                            <li><label><input type="checkbox" name="categories[]" value="0" >default</label></li>
                        @endforelse
                    </ul>
                </div>
            </div>


            <!--INFO-->
            @include('admin.product.info.info')




        </div>
    </div>

</form>

<script>
    if(typeof tinymce.init){
        tinymce.init(
            {
                selector:'.text',
                theme: 'modern',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
                image_advtab: true
            }
        )
    }
</script>

@endsection
