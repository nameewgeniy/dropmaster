<div class="uk-panel uk-panel-box " >
    <div class="uk-grid">
        <div class="uk-width-1-1">
            <legend>Info product</legend>
        </div>
        <div class="uk-width-1-2">
            <label for="">
                Rate
                <input type="text"  name="rate" value="{{ $product->rate or '' }}"/>
            </label>
        </div>
        <div class="uk-width-1-2">
            <label for="">
                Orders
                <input type="text"  name="orders" value="{{ $product->orders or '' }}"/>
            </label>
        </div>
        <div class="uk-width-1-1 uk-margin-top">
            @if(!is_null($product->link))
                <p>Link to product store: <a href="{{ $product->link }}">{{ $product->title }}</a></p>
                <input type="hidden"  name="link" value="{{ $product->link or '' }}"/>
            @endif
        </div>
    </div>
</div>
