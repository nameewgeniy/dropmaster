<table  class="uk-table variations-table" {!! empty($variations)? 'style="display: none"' : '' !!} >
    <thead>
        <tr>
            <th></th>
            <th>Variant</th>
            <th>Image</th>
            <th>SKU</th>
            <th>Inventory</th>
            <th>
                Price
            </th>
            <th>
                <fieldset data-uk-margin>
                    <span>Rate: </span>
                    <input type="text" class="uk-form-width-mini" id="rate"/>
                    <button type="button" class="uk-button uk-button-primary" onclick="Options.calc()" >Calc</button>
                </fieldset>
            </th>
        </tr>
    </thead>
    <tbody>
        @isset($variations)
            @foreach($variations as $variation)
                <tr>
                    <td>
                        <input type="checkbox" checked name="variations[{{ $loop->iteration }}][check]"/>
                    </td>
                    <td>
                        {{ $variation['name'] }}
                        <input type="hidden" value="{{ $variation['name'] }}" name="variations[{{ $loop->iteration }}][name]" />
                    </td>
                    <td>
                        <div class="def-img var-img"
                            onclick="Images.init(this,'option')"
                            {!! !is_null($variation['img'])?  'style="background-image: url(' . Images::getImageByID($variation['img']) . ')"' : '' !!}
                         >
                            <input type="hidden"  value="{{ $variation['img'] }}" name="variations[{{ $loop->iteration }}][img]" />
                        </div>
                    </td>
                    <td>
                        <input type="text" readonly   value="{{ $variation['sku'] }}" name="variations[{{ $loop->iteration }}][sku]"/>
                    </td>
                    <td>
                        <input type="text"  value="{{ $variation['count'] }}" name="variations[{{ $loop->iteration }}][count]"/>
                    </td>
                    <td>
                        <input type="text" data-price="{{ $variation['price'] }}" class="price-option" value="{{ $variation['price'] }}" placeholder="$0.00" name="variations[{{ $loop->iteration }}][price]" />
                    </td>
                    <td>

                    </td>
                </tr>
            @endforeach
        @endisset
    </tbody>
</table>