<div class="uk-panel uk-panel-box price-product">
    <legend>Pricing</legend>
    <div class="uk-grid">
        <div class="uk-width-1-2">
            <label for="">Price</label>
            <input type="text" placeholder="$ 0.00" {!! empty($default)? 'disabled="disabled"' : '' !!}   name="default[price]" value="{{ $default['price'] or '' }}" class="uk-width-1-1">
        </div>
        <div class="uk-width-1-2">
            <label for="">Inventory</label>
            <input type="text" placeholder="0" {!! empty($default)? 'disabled="disabled"' : '' !!} name="default[count]" value="{{ $default['count'] or '' }}" class="uk-width-1-1">
        </div>
    </div>
</div>
