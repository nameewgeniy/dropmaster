@extends('admin.index')
@section('main')
    <h3>All Products</h3>
    <table class="uk-table">
        <thead>
        <tr>
            <th>№</th>
            <th>Image</th>
            <th>Product name</th>
            <th>Price</th>
            <th>Status</th>
            <th>Date</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse( Product::orderBy('created_at', 'desc')->get() as $product)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                    @if(!$product->images->isEmpty())
                        <img src="{{ Images::getImageByID($product->images->first()->id) }}" width="50" height="50" alt=""/>
                    @endif
                </td>
                <td>{{ $product->title }}</td>
                <td><strong>{{ $product->getBestPrice() }} {{ Setting::getCurrency() }}</strong></td>
                <td>{{ ($product->status == 1)? 'Publish' : 'No publish' }}</td>
                <td>{{ $product->created_at->format('d-m-Y') }}</td>
                <td>
                    <a href="{{ url('admin/product/new/' . $product->id) }}" class="uk-button-primary uk-button">
                        <i class="uk-icon-edit" ></i>
                    </a>
                    <a href="{{ url('admin/product/delete/' . $product->id) }}" class="uk-button-danger uk-button">
                        <i class="uk-icon-trash-o" ></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                Products not found((
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
