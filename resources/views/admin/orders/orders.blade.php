@extends('admin.index')
@section('main')
<h3>All Orders</h3>
<div class="uk-accordion" data-uk-accordion="{ showfirst: false }">

    @forelse($orders as $order)
    <form action="{{ url('admin/orders/update/' . $order->id)}}" class="uk-form">

        <div class="uk-grid uk-accordion-title" style="background: none!important; margin-left: 0">
            <div class="uk-width-1-6"><p><strong>Status: </strong>{{ $order->status }}</p></div>
            <div class="uk-width-1-6"><p><strong>Payment:</strong> {{ $order->price }}</p></div>
            <div class="uk-width-2-6"><p><strong>Date order: </strong>{{ $order->created_at }}</p></div>
        </div>

        <div class="uk-accordion-content uk-grid uk-panel ">
            <div class="uk-width-1-5">
                @include('common.orders-client', [ 'client' => json_decode($order->client, true) ])
            </div>
            <div class="uk-width-4-5">
                <div class="uk-overflow-container uk-panel uk-panel-box uk-padding" >
                    <table class="uk-table uk-table-condensed ">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                                <th>Track</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                            <tbody>
                                @isset($order->sales)
                                    @foreach($order->sales as $product)
                                        <tr>
                                            <td>
                                                <div class="product-cart uk-grid uk-margin-remove">
                                                    <div class="img uk-width-2-10" >
                                                        <img
                                                            src="{{ is_null($product['img']) ?
                                                                Images::getImageByID(Product::getFirstImg($product['product_id']),'50x50') :
                                                                Images::getImageByID($product['img'],'50x50') }}"
                                                            style="" alt=""/>
                                                    </div>
                                                    <div class="product-info-cart uk-width-8-10">
                                                        <p>
                                                            <a href="{{ url('product/' . Product::whereId($product['product_id'])->pluck('slug')->first()) }}">
                                                                {{ $product['name'] }}
                                                            </a><br/>
                                                            @if(!is_null(Product::whereId($product['product_id'])->pluck('link')->first()))
                                                                <a href="{{ Product::whereId($product['product_id'])->pluck('link')->first() }}">
                                                                    Product link to store ->
                                                                </a>
                                                            @endif
                                                        </p>
                                                        <p class="uk-text-muted">
                                                            {{ $product['variation'] }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td >{{ $product['count'] }} pieces</td>
                                            <td><strong>{{ $product['price']   }} {{ $product['currency'] or '' }} / one item</strong></td>
                                            <td><input type="text" name="info[{{$product['id']}}][track]" value="{{ $product['track'] or '' }}" /></td>
                                            <td>
                                                <input type="text" name="info[{{$product['id']}}][details]" value="{{ $product['details'] or '' }}"/>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                    </table>
                    <strong>Status order:</strong>
                    <select name="status" id="">
                        <option value="{{ $order->status }}">{{ $order->status }}</option>
                        <option value="processed">processed</option>
                        <option value="delivered">delivered</option>
                        <option value="sent">sent</option>
                        <option value="closed">closed</option>
                        <option value="paid">paid</option>
                    </select>
                    <br/>
                    <strong>Key: </strong> {{ $order->key }}
                    <button type="submit" class="uk-button uk-button-success uk-float-right" >Update</button>
                    <a href="{{ url('admin/orders/delete/' . $order->id) }}" class="uk-button uk-button-danger uk-float-right uk-margin-right">Delete</a>
                </div>
            </div>
        </div>
    </form>

    @empty
        <p>Orders not found</p>
    @endforelse

</div>
@endsection
