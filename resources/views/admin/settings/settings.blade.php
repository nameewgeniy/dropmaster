@extends('admin.index')
@section('main')
<h3>Settings</h3>
<form class="uk-grid uk-form" method="post" >
    {{ csrf_field() }}
    <div class="uk-width-1-2">

        <div class="uk-panel uk-panel-box ">

            <legend class="">Template</legend>

            <label class="uk-display-block" style="padding-bottom: 15px" >
                Name store
                <input
                    type="text"
                    placeholder=""
                    class="uk-width-1-1 uk-form-large"
                    name="settings[store]"
                    value="{{ Setting::getSetting('store') }}"
                    >
            </label>

            <label class="uk-display-block" >
                <h5>Count categories on home</h5>
                <select name="settings[home_cat]" class="uk-select uk-width-1-2 uk-margin-bottom" id="">
                    @forelse(Categories::all() as $cat)
                        <option value="{{ $loop->iteration }}" {{ Functions::selected($loop->iteration, Setting::getSetting('home_cat') ) }} >
                        {{ $loop->iteration }}
                    </option>
                    @empty
                        <option value="0">0</option>
                    @endforelse
                </select>
            </label>

        </div>

        <div class="uk-panel uk-panel-box">
            <legend class="">Payment</legend>
            <label for="">
                <h5>Currency</h5>
                <select name="settings[currency]" class="uk-width-1-2 uk-margin-bottom" >
                    <option value="0" {{ Functions::selected(0, Setting::getSetting('currency'), 'selected' ) }}  >USD</option>
                    <option value="1" {{ Functions::selected(1, Setting::getSetting('currency') ) }}  >RUB</option>
                </select>
                <!--<input
                    type="text"
                    placeholder=""
                    class="uk-width-1-1 uk-form-large"
                    name=""
                    value="{{ Setting::getSetting('currency') or 'USD' }}"
                    >-->
            </label>
        </div>

        <div class="uk-panel uk-panel-box ">

            <legend class="">Contact Us</legend>

            <label class="uk-display-block" style="padding-bottom: 15px" >
                <h5>Address</h5>
                <textarea name="settings[address]" class="uk-width-1-1">{{ Setting::getSetting('address') }}</textarea>
            </label>

            <label class="uk-display-block" style="padding-bottom: 15px">
                Admin Email
                <input
                    type="text"
                    placeholder=""
                    class="uk-width-1-1 uk-form-large"
                    name="settings[email]"
                    value="{{ Setting::getSetting('email') }}"
                    >
            </label>
        </div>

        <div class="uk-panel uk-panel-box">
            <legend class="">Social</legend>

            <label class="uk-display-block" style="padding-bottom: 15px">
                Facebook
                <input
                    type="text"
                    placeholder=""
                    class="uk-width-1-1 uk-form-large"
                    name="settings[facebook]"
                    value="{{ Setting::getSetting('facebook') }}"
                    >
            </label>

            <label class="uk-display-block" style="padding-bottom: 15px" >
                Twitter
                <input
                    type="text"
                    placeholder=""
                    class="uk-width-1-1 uk-form-large"
                    name="settings[twitter]"
                    value="{{ Setting::getSetting('twitter') }}"
                    >
            </label>

            <label class="uk-display-block" style="padding-bottom: 15px" >
                Instagram
                <input
                    type="text"
                    placeholder=""
                    class="uk-width-1-1 uk-form-large"
                    name="settings[instagram]"
                    value="{{ Setting::getSetting('instagram') }}"
                    >
            </label>

            <label class="uk-display-block" style="padding-bottom: 15px" >
                Pinterest
                <input
                    type="text"
                    placeholder=""
                    class="uk-width-1-1 uk-form-large"
                    name="settings[pinterest]"
                    value="{{ Setting::getSetting('pinterest') }}"
                    >
            </label>

        </div>

        <div class="uk-panel uk-panel-box ">

            <legend class="">Gallery</legend>

            <label class="uk-display-block" style="padding-bottom: 15px" >
                <h5>Gallery</h5>
                <textarea name="settings[gallery]" class="uk-width-1-1">{{ Setting::getSetting('gallery') }}</textarea>
            </label>

        </div>

        <div class="uk-panel uk-panel-box ">

            <legend class="">Footer scripts</legend>

            <label class="uk-display-block" style="padding-bottom: 15px" >
                <textarea name="settings[scripts]" rows="10" class="uk-width-1-1">{!! Setting::getSetting('scripts') !!}</textarea>
            </label>

        </div>

        <div class="uk-panel uk-panel-box">
            <button class="uk-button uk-button-success uk-float-right" type="submit">Save</button>
        </div>
    </div>
    <div class="uk-width-1-2"></div>
</form>
@endsection
