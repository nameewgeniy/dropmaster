@extends('admin.index')
@section('main')
<h3>All Post</h3>
<table class="uk-table">
    <thead>
    <tr>
        <th>Post name</th>
        <th>Post status</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @forelse($posts as $post)
    <tr>
        <td>{{ $post->title }}</td>
        <td>{{ ($post->status == 1)? 'Publish' : 'No publish' }}</td>
        <td>
            <a href="{{ url('admin/post/new/' . $post->id) }}" class="uk-button-primary uk-button">
                <i class="uk-icon-edit" ></i>
            </a>
            <a href="{{ url('admin/post/delete/' . $post->id) }}" class="uk-button-danger uk-button">
                <i class="uk-icon-trash-o" ></i>
            </a>
        </td>
    </tr>
    @empty
        <tr>
            Posts not found((
        </tr>
    @endforelse
    </tbody>
</table>
@endsection
