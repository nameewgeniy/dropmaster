@extends('admin.index')
@section('main')
<script type="application/javascript" src="//cdn.tinymce.com/4/tinymce.min.js" ></script>
<h3>New Post </h3>

<form class="uk-form" id="new-post" method="post">
    {{ csrf_field() }}
    <div class="uk-clearfix uk-margin">
        <button type="submit" class="uk-button uk-float-right uk-button-success"><i class="uk-icon-save "></i>  Save</button>
        <select name="status" class="uk-float-right uk-margin-right" >
            <option value="1" @isset($post->status) {{ Functions::selected(1, $post->status ) }} @endisset >Publish</option>
            <option value="0" @isset($post->status) {{ Functions::selected(0, $post->status ) }} @endisset >No publish</option>
        </select>
    </div>
    <div class="uk-grid uk-grid-divider">
        <div class="uk-width-1-2 ">
            <div class="uk-panel uk-panel-box">
                <div class="uk-panel-badge uk-badge uk-badge-success">TITLE</div>
                <input
                    type="text"
                    placeholder="Title"
                    class="uk-width-1-1 uk-form-large"
                    name="title"
                    value="{{ $post->title or '' }}"
                >
            </div>
            <div class="uk-panel uk-panel-box">
                <legend class="uk-margin-top">Post content</legend>
                <textarea class="text uk-margin-top" style="height: 300px;" name="content" id="content" >{!! $post->text or '' !!}</textarea>
            </div>
        </div>
        <div class="uk-width-1-2 ">
            <div class="uk-panel uk-margin-bottom uk-panel-box">
                <div class="uk-panel-badge uk-badge uk-badge-success">H1</div>
                <input type="text" placeholder="h1" class="uk-width-1-1 uk-form-large" name="h1" value="{{ $post->h1 or '' }}">
            </div>

            <div class="uk-grid " style="padding-bottom: 20px">
                <div class="uk-width-1-2 ">
                    <div class="uk-panel uk-panel-box">
                        <legend>Keywords</legend>
                        <textarea style="height: 80px;" class="uk-width-1-1" name="keywords"  >{!! $post->keywords or '' !!}</textarea>
                        <legend>Description</legend>
                        <textarea style="height: 80px;" class="uk-width-1-1" name="description"  >{!! $post->description or '' !!}</textarea>
                    </div>
                </div>
                <div class="uk-width-1-2 uk-grid-divide ">
                    <div class="uk-panel uk-panel-box">
                        <legend>Categories</legend>
                        <div class="uk-scrollable-box" style="height:205px">
                            <ul class="uk-list">
                                @forelse($categories as $category)
                                <li>
                                    <label>
                                        <input
                                            type="checkbox"
                                            name="categories[]"
                                            value="{{ $category->id }}"

                                        @isset($post->categories)
                                        @if(in_array( (string)$category->id, (array)$post->categories()->pluck('category_id')->toArray() ))
                                        checked="checked"
                                        @endif
                                        @endisset
                                        >
                                        {{ $category->name }}
                                    </label>
                                </li>
                                @empty
                                <li><label><input type="checkbox" name="categories[]" value="0" >default</label></li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-panel uk-panel-box">
                <legend class="">Thumbnail</legend>
                <div class="uk-thumbnail uk-thumbnail-medium uk-margin-bottom"  id="thumbnail">
                    @if(isset($post->thumbnail_id))
                    <img src="{{ App\Models\Image::getImageByID($post->thumbnail_id) }}" alt=""/>
                    @else
                    <p class="uk-text-center" >No thumbnail</p>
                    @endif
                </div>
                <br/>
                <button class="uk-button" type="button" onclick="Images.init(this, 'post')" >Add thumbnail</button>
                <input type="hidden" name="thumbnail" value="{{ $post->thumbnail_id or '' }}"/>
            </div>
        </div>
    </div>

</form>

<script>
    tinymce.init(
        {
            selector:'.text',
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true
        }
    )
</script>

@endsection
