@extends('admin.app')

@section('content')
<section class="uk-grid">
    <div class="uk-block uk-block-muted uk-container-center register">
        <h3 class="uk-text-center">Register</h3>
        <form class="uk-form " method="POST" action="{{ route('register') }}">
            <fieldset data-uk-margin>
                {{ csrf_field() }}
                <div class="uk-form-row" >
                    <legend>Name</legend>
                    <input id="name" type="text" class="uk-form-width-large"  name="name" value="{{ old('name') }}" required autofocus>
                </div>
                <div class="uk-form-row" >
                    <legend>Email</legend>
                    <input id="email" type="email" class="uk-form-width-large"  name="email" value="{{ old('email') }}" required>
                </div>
                <div class="uk-form-row" >
                    <legend>Password</legend>
                    <input id="password" type="password" class="uk-form-width-large"  name="password" required>
                </div>
                <div class="uk-form-row" >
                    <legend>Confirm password</legend>
                    <input id="password-confirm" type="password" class="uk-form-width-large"  name="password_confirmation" required>
                </div>
                <div class="uk-form-row" >
                    <button type="submit" class="uk-button">
                        Register
                    </button>
                </div>
            </fieldset>
        </form>
    </div>
</section>
@endsection
