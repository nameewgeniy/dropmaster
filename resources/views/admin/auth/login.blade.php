@extends('admin.app')

@section('content')
<section class="uk-grid uk-cover-background uk-height-viewport" style="background-image: url({{ asset('bg.jpg') }});">
    <div class="uk-block uk-block-secondary uk-container-center register" style="max-height: 260px; background: #fff">
        <form class="uk-form uk-form-large" method="POST" action="{{ route('login') }}">
            <fieldset data-uk-margin>
                {{ csrf_field() }}

                <div class="uk-form-row" >
                    <legend>Email</legend>
                    <input id="email" type="email" class="uk-form-width-large"  name="email" value="{{ old('email') }}" required>
                </div>
                <div class="uk-form-row" >
                    <legend>Password</legend>
                    <input id="password" type="password" class="uk-form-width-large"  name="password" required>
                </div>
                <div class="uk-form-row" >
                    <button type="submit" class="uk-button uk-float-right uk-margin-top">
                        Login
                    </button>
                </div>

            </fieldset>
        </form>
    </div>
</section>
@endsection
