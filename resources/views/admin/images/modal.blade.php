@forelse ($images as $image)
    <div>
        <label class="uk-button img uk-display-block uk-margin-bottom"
               style="
                background: url({{ App\Models\Image::getImageBySrc($image->src, '220x220') }}) #fff no-repeat;
                background-size: contain;
                "
               id="thm{{ $image->id }}"
               data-src="{{ App\Models\Image::getImageBySrc($image->src, '220x220') }}"
               data-value="{{ $image->id }}"
            >
            <i class="uk-icon-check-square"></i>
        </label>
    </div>

@empty
<p class="uk-text-center">Images not found</p>
@endforelse

