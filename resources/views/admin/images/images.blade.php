@extends('admin.index')
@section('main')

<div class="uk-panel">
    <h3 class="uk-panel-title">Images</h3>
    <a href="{{ url('admin/images/delete/all') }}" class="uk-button uk-button-danger">Delete All</a>
</div>
<div id="upload-drop" class="uk-placeholder">
    Drop image files here... <a class="uk-form-file">Select a file<input id="upload-select" type="file"></a>.
</div>

<div id="progressbar" class="uk-progress uk-hidden">
    <div class="uk-progress-bar" style="width: 0%;">...</div>
</div>

<form class="all-images uk-grid">
@forelse ($images as $image)
    <label class=" uk-width-1-6 uk-margin-bottom " for="{{ $image->id }}">
        <img src="{{ App\Models\Image::getImageBySrc($image->src, '220x220') }}" class="uk-thumbnail" alt="">
        <div class="uk-thumbnail-caption uk-clearfix" >
            <input type="text" class="uk-button uk-width-1-1 uk-margin-bottom" value="{!! App\Models\Image::getImageBySrc($image->src, '220x220') !!}" readonly />
            <a href="{{ url('admin/images/delete/' . $image->id ) }}" class="uk-button-small uk-button uk-button-danger uk-float-left">Delete</a>
        </div>
        <hr style="clear: both"/>
    </label>
    <input type="checkbox" id="{{ $image->id }}" style="display: none"/>
@empty
    <p>Images not found</p>
@endforelse
</form>
<ul class="uk-pagination">
    @for($i = 1; $i <= ceil($images->total()/$images->perPage()); $i++)
        <li><a href="{{ url()->current() }}?page={{ $i }}">{{ $i }}</a></li>
    @endfor
</ul>


<script defer="defer" src="{{ asset('files/js/components/upload.min.js') }}"></script>
@endsection
