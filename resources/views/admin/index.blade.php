@extends('admin.app')
@section('content')
<div class="uk-grid">
    <section class="left-menu uk-width-medium-2-10 ">
        <div class="uk-panel uk-panel-box uk-height-viewport">

            <h3 class="uk-panel-title">Admin</h3>

            <ul class="uk-nav uk-nav-side uk-nav-parent-icon" data-uk-nav="">
                <li class="{{ url()->current() == url('admin')? 'uk-active' : '' }}"><a href="{{ url('admin') }}"><i class="uk-icon-pie-chart"></i> Dashboard</a></li>
                <li class="uk-nav-divider"></li>
                <li class="uk-parent" aria-expanded="false">
                    <a href="#"><i class="uk-icon-file-text"></i> Posts</a>
                    <ul class="uk-nav-sub" role="menu">
                        <li>
                            <ul>
                                <li><a href="{{ url('admin/post') }}">Posts</a></li>
                                <li class="{{ url()->current() == url('admin/post/new')? 'uk-active' : '' }}"><a href="{{ url('admin/post/new') }}">New post</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>

                <li class="uk-parent" aria-expanded="false">
                    <a href="#"><i class="uk-icon-shopping-cart"></i> Products</a>
                    <ul class="uk-nav-sub" role="menu">
                        <li>
                            <ul>
                                <li><a href="{{ url('admin/product') }}">Products</a></li>
                                <li><a href="{{ url('admin/product/new') }}">New product</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li class="{{ url()->current() == url('admin/categories')? 'uk-active' : '' }}" ><a href="{{ url('admin/categories') }}"><i class="uk-icon-list-ul"></i> Categories</a></li>
                <li class="{{ url()->current() == url('admin/coupon')? 'uk-active' : '' }}" ><a href="{{ route('coupons') }}"><i class="uk-icon-money"></i> Coupons</a></li>
                <li class="{{ url()->current() == url('admin/subscribes')? 'uk-active' : '' }}" ><a href="{{ route('subscribes') }}"><i class="uk-icon-rss"></i> Subscribes</a></li>
                <li class="{{ url()->current() == url('admin/images')? 'uk-active' : '' }}" ><a href="{{ url('admin/images') }}"><i class="uk-icon-image "></i> Images</a></li>
                <li class="{{ url()->current() == url('admin/orders')? 'uk-active' : '' }}" ><a href="{{ url('admin/orders') }}"><i class="uk-icon-credit-card "></i> Orders</a></li>
                <li class="uk-nav-divider"></li>
                <li class="{{ url()->current() == url('admin/settings')? 'uk-active' : '' }}" ><a href="{{ url('admin/settings') }}"><i class="uk-icon-gears "></i> Settings</a></li>
            </ul>
        </div>
    </section>
    <section id="content" class="uk-width-medium-8-10" >
        @if( $errors->any())
            @foreach( $errors->all() as $error )
                <div class="uk-alert uk-alert-dancer" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p>{{ $error }}</p>
                </div>
            @endforeach
        @endif
        @yield('main')
    </section>
</div>

@endsection
