@extends('admin.index')
@section('main')
<h3>Categories</h3>
<form class="uk-form categories" action="{{ url('admin/categories/new') }}" method="post">
    {{ csrf_field() }}
    <table class="uk-table">
        <thead>
        <tr>
            <th>Category Name</th>
            <th>Category Slug</th>
            <th>Parent Category</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><input type="text" name="name" placeholder="category name" onkeyup="liveGenerateSlug(this)"></td>
            <td><input type="text" name="slug" placeholder="category slug"></td>
            <td>
                <select name="parent_id" >
                    <option value="0">Root</option>
                    @forelse($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @empty
                        {{ '' }}
                    @endforelse
                </select>
            </td>
            <td><input type="submit" value="save" class="uk-button-success uk-button"/></td>
        </tr>
        @forelse($categories as $category)
            <tr>
                <td>{{ $category->name }}</td>
                <td>{{ $category->slug }}</td>
                <td>
                    @foreach($categories as $parent)
                        @if($parent->id == $category->parent_id)
                            {{ $parent->name }}
                        @endif
                    @endforeach
                </td>
                <td>
                    <a href="{{ url('admin/categories/edit/' . $category->id) }}" class="uk-button-primary uk-button">
                        <i class="uk-icon-edit" ></i>
                    </a>
                    <a href="{{ url('admin/categories/delete/' . $category->id) }}" class="uk-button-danger uk-button">
                        <i class="uk-icon-trash-o" ></i>
                    </a>
                </td>
            </tr>
        @empty
            <p>Categories not found</p>
        @endforelse
        </tbody>
    </table>
</form>

@endsection
