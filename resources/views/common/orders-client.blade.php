@isset($client)
    <div class="uk-panel  uk-panel-box uk-panel-box-primary">
        <div class="uk-panel-badge uk-badge">Client</div>
        @if(!isset($secret))
            <p><strong>Email: </strong>{{ $client['email'] }}</p>
            <p><strong>Name: </strong>{{ $client['name'] or '' }}</p>
            <p><strong>Address: </strong>{{ $client['address'] }}</p>
            <p><strong>Etc: </strong>{{ $client['etc'] or ''}}</p>
            <p><strong>City: </strong>{{ $client['city'] }}</p>
            <p><strong>Country: </strong>{{ $client['country'] }}</p>
            <p><strong>Region: </strong>{{ $client['region'] }}</p>
            <p><strong>Code: </strong>{{ $client['code'] }}</p>
            <p><strong>Phone: </strong>{{ $client['phone'] or '' }}</p>
            <p><strong>Details: </strong>{{ $client['details'] or '' }}</p>
        @else
            <p><strong>Email: </strong>{{ Functions::maskData($client['email']) }}</p>
            <p><strong>Name: </strong>{{ Functions::maskData($client['name']) }}</p>
            <p><strong>Address: </strong>{{ Functions::maskData($client['address']) }}</p>
            <p><strong>Etc: </strong>{{  Functions::maskData($client['etc']) }}</p>
            <p><strong>City: </strong>{{ $client['city'] }}</p>
            <p><strong>Country: </strong>{{ $client['country'] }}</p>
            <p><strong>Region: </strong>{{ $client['region'] }}</p>
            <p><strong>Code: </strong>{{ $client['code'] }}</p>
            <p><strong>Details: </strong>{{ Functions::maskData($client['details']) }}</p>
        @endif
    </div>
@endisset