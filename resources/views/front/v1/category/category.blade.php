@extends('front.v1.index')

@section('main')
    <section id="category" class="width1140 uk-grid">
        <ul class="uk-breadcrumb uk-width-1-1 uk-margin-bottom">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="uk-text-muted">{{ $category->name }}</li>
        </ul>
        <div class="categories-sidebar uk-width-large-2-10 uk-width-medium-2-10 uk-hidden-small">
            @include('front.v1.category.categories', ['count' => 100])
        </div>
        <div class="slide-home-main uk-width-large-8-10 uk-width-medium-8-10 uk-width-small-1-1 uk-margin-remove-right" >
            <h2>{{ $category->name }}</h2>
            <p>Sort by:
                <a href="{{ url()->current() }}?sortBy=rate">Best Match <i class="ion-ios-arrow-thin-up"></i></a>
                <a href="{{ url()->current() }}?sortBy=orders">Popularity <i class="ion-ios-arrow-thin-up"></i></a>
            </p>
            <hr/>
            <div class="uk-grid uk-grid-medium list-product">
                @include('front.v1.include.products', [ 'products' => $category->products->sortByDesc($sort), 'column' => '1-3' ])
            </div>
        </div>
    </section>

@endsection