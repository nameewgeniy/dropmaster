@isset($categories)
    <ul class="uk-nav uk-nav-side">
        @foreach( $categories->take($count) as $cat)
            <li><a href="{{ url('category/' . $cat->slug) }}">{{ $cat->name }}</a></li>
        @endforeach
    </ul>

@endisset

