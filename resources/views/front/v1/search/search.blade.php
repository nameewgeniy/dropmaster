@extends('front.v1.index')

@section('main')
    <section id="category" class="width1140 uk-grid">
        <ul class="uk-breadcrumb uk-width-1-1 uk-margin-bottom">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="uk-text-muted">Search Results for "{{ $skey }}"</li>
        </ul>
        <div class="categories-sidebar
        uk-width-large-2-10
        uk-visible-large">
            @include('front.v1.category.categories',['count' => 100])
        </div>
        <div class="slide-home-main
         uk-width-large-8-10
        uk-width-medium-1-1
        uk-width-small-1-1
         uk-margin-remove-right" >
            <div class="uk-grid uk-grid-medium list-product">
                @include('front.v1.include.products', [ 'column' => '1-3', 'empty' => "Sorry, but products not found" ])
            </div>
        </div>
    </section>

@endsection