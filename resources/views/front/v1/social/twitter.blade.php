<a
    class="twitter-timeline"
    href="https://twitter.com/{{ Setting::getSetting('twitter') }}?ref_src=twsrc%5Etfw"
    data-height="500"
    >
        Tweets by {{ Setting::getSetting('twitter') }}
</a>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>