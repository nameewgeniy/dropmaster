<div class="fb-page"
     data-href="https://www.facebook.com/{{ Setting::getSetting('facebook') }}/"
     data-tabs="timeline"
     data-small-header="true"
     data-adapt-container-width="true"
     data-hide-cover="true"
     data-show-facepile="true">
    <blockquote
        cite="https://www.facebook.com/{{ Setting::getSetting('facebook') }}/"
        class="fb-xfbml-parse-ignore">
        <a href="https://www.facebook.com/{{ Setting::getSetting('facebook') }}/"></a>
    </blockquote>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=513152402208574";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>