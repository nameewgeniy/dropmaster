
<meta property="og:title" content="{{ $title or config('app.name') }}"/>
<meta property="og:site_name" content="{{ config('app.name') }}" />
<meta property="og:url" content="{{ secure_url($_SERVER['REQUEST_URI']) }}" />
<meta property="og:description" content="{{ $title or $product->text }}" />
<meta property="og:image" content="{{ isset($product)? Images::getImageByID($product->images[0]->id) : url('images/logo.png') }}" />

<meta name="twitter:card" content="summary"/>
<meta name="twitter:site" content="{{ '@'.Setting::getSetting('twitter') }}"/>
<meta name="twitter:domain" content="{{ secure_url('/') }}"/>
<meta name="twitter:title" content="{{ $title or config('app.name') }}"/>
<meta name="twitter:description" content="{{ $title or $product->text }}"/>
<meta name="twitter:image:src" content="{{ isset($product)? Images::getImageByID($product->images[0]->id) : url('images/logo.png') }}" />