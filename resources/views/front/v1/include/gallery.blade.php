@if(!is_null(Setting::getGallery()))
    <div class="uk-slidenav-position" data-uk-slideshow="{kenburns:'15s', autoplay: true, animation: 'scale', duration: 250, height: 402}">
        <ul class="uk-slideshow">
            @foreach(Setting::getGallery() as $image)
            <li><img src="{{ $image }}" alt=""/></li>
            @endforeach
        </ul>
        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
        <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
            @foreach(Setting::getGallery() as $image)
            <li data-uk-slideshow-item="{{ $loop->iteration }}"><a href=""></a></li>
            @endforeach
        </ul>
    </div>
@endif