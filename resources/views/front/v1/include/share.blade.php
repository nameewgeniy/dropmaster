<div class="share" style="text-align: right">
    <script async type="text/javascript">(function(w,doc) {
            if (!w.__utlWdgt ) {
                w.__utlWdgt = true;
                var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
                var h=d[g]('body')[0];
                h.appendChild(s);
            }})(window,document);
    </script>

    <div class="row" style="border-top: 1px solid #ddd; padding-top: 10px; margin: 15px 0;">
        <div
            data-background-alpha="0.0"
            data-buttons-color="#FFFFFF"
            data-counter-background-color="#ffffff"
            data-share-counter-size="12"
            data-top-button="false"
            data-share-counter-type="common"
            data-share-style="8"
            data-mode="share"
            data-like-text-enable="false"
            data-mobile-view="false"
            data-icon-color="#ffffff"
            data-orientation="horizontal"
            data-text-color="#000000"
            data-share-shape="rectangle"
            data-sn-ids="fb.tw.gp.ps.tm"
            data-share-size="30"
            data-background-color="#ffffff"
            data-preview-mobile="false"
            data-mobile-sn-ids="fb.tw.gp.ps.tm"
            data-pid="1434094"
            data-counter-background-alpha="1.0"
            data-following-enable="false"
            data-exclude-show-more="true"
            data-selection-enable="false"
            class="uptolike-buttons" ></div>
    </div>
</div>