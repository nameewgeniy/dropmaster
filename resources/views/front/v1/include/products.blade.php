@forelse( $products as $product )
    <div class="
    uk-width-large-{{ $column or '1-4' }}
    uk-width-medium-2-4
    uk-width-small-1-1
    product">
        <a class="uk-thumbnail" href="{{ url('product/' . $product->slug) }}">
            <div class="img-product" style="background-image: url({{ Images::getImageByID($product->images[0]->id) }})">
            </div>
            <div class="uk-thumbnail-caption">
                <p class="title">
                    {{ $product->title }}
                </p>

                <section class="price">
                    <p class="short-stars">
                        @isset($product->rate)
                                @for ($i = 0; $i < $product->rate; $i++)
                                <span class="uk-icon-star"></span>
                                @endfor

                                @for ($i = 0; $i < 5 - $product->rate; $i++)
                                <span class="uk-icon-star-o"></span>
                                @endfor
                                <span style="color: #8BBD2B" >({{ $product->orders or '325' }})</span>
                        @endisset
                    </p>
                    <span class="price-text" >Price:</span>

                    <span class="price-value">{{ $product->getBestPrice() }} {{ Setting::getCurrency() }}</span>
                </section>
                <p class="more">
                    <button class="uk-button">More</button>
                </p>
            </div>
        </a>
    </div>
@empty
        <div class="uk-alert uk-alert-warning uk-width-1-1">
            <h2 class="uk-text-center " >{{ $empty or 'Sorry, but products not found' }}</h2>
        </div>

@endforelse