@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Page not found</li>
    </ul>

    <i class="uk-icon-exclamation-circle uk-text-center" style="display: block; font-size: 110px; color: #F8981F"  ></i>
    <h1 class="uk-text-center" >Sorry... page not found. 404 error.</h1>
    <div class="uk-text-center">
        <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
    </div>
    <br/>
</section>
@endsection