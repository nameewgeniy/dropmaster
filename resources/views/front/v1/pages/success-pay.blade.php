@extends('front.v1.index')

@push('scripts')
    <script>
        jQuery.cookie('cart', null,{ path: '/', expires: 1});
        jQuery.cookie('count', 0,{ path: '/', expires: 1});
    </script>
@endpush

@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Thank you</li>
    </ul>

    <i class="uk-icon-check-circle uk-text-center" style="display: block; font-size: 110px; color: #6CB15E"  ></i>
    <h1 class="uk-text-center" >You're all set!</h1>
    <h4 class="uk-text-center" >View order status: <a href="{{ url('order/status?key=') }}{{ $order or '' }}">{{ $order or '' }}</a></h4>
    <h2 class="uk-text-center" >Thanks for being awesome,<br/> we hope you enjoy your purchase!</h2>
    <div class="uk-text-center">
        <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
    </div>
    <br/>
</section>
@endsection