@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Refunds & Returns Policy</li>
    </ul>

    <h2>Refunds & Returns Policy</h2>
    <hr/>

    <h4><strong>Order cancellation</strong></h4>
    <p>All orders can be cancelled until they are shipped. If your order has been paid and you need to make a change or cancel an order, you must contact us within 12 hours. Once the packaging and shipping process has started, it can no longer be cancelled.</p>

    <h4><strong>Refunds</strong></h4>
    <p>Your satisfaction is our #1 priority. Therefore, if you’d like a refund you can request one no matter the reason.</p>

    <p>If you did <strong>not</strong> receive the product within the guaranteed time(45 days not including 2-5 day processing) you can request a refund or a reshipment.</p>

    <p>If you received the wrong item you can request a refund or a reshipment.</p>

    <p>If you do not want the product you’ve receive you may request a refund but you must return the item at your expense and the item must be unused.</p>

    <ul>
        <li>Your order did not arrive due to factors within your control (i.e. providing the wrong shipping address)</li>
        <li>Your order did not arrive due to exceptional circumstances outside the control of <span class="uk-blend-color" >{{ config('app.name') }}</span> (i.e. not cleared by customs, delayed by a natural disaster).</li>
        <li>Other exceptional circumstances outside the control of <a href="{{ url('/') }}">{{ config('app.name') }}</a></li>
    </ul>

    <small class="uk-article-meta" >*You can submit refund requests within 15 days after the guaranteed period for delivery (45 days) has expired. You can do it by sending a message on Contact Us page</small>

    <p>If you are approved for a refund, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within 14 days.</p>

    <h4><strong>Exchanges</strong></h4>
    <p>If for any reason you would like to exchange your product, perhaps for a different size in clothing. You must contact us first and we will guide you through the steps.</p>

    <p>Please do not send your purchase back to us unless we authorise you to do so.</p>

    <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
</section>
@endsection