@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Privacy Policy</li>
    </ul>

    <h2>Privacy Policy</h2>
    <hr/>

    <p>Your satisfaction is our long-term pursuit and concern. We want you to shop with confidence. That’s why we provide guarantees that ensure you’ll receive your item on time and as described.</p>

    <p>Everyone who shops on our {{ config('app.name') }} receives the following guarantees:</p>

    <p>Full Refund if you don’t receive your order.</p>

    <p>You will get a full refund if your order does not arrive within the delivery time promised by our {{ config('app.name') }}.</p>

    <p>Full or Partial Refund if the item is not as described.</p>

    <p>If your item is significantly different from the product description, you can A: Return it and get a full refund,
        or B: Get a partial refund and keep the item.</p>

    <p>Full refunds are not available under the following circumstances:</p>

    <p>1. Your order did not arrive due to factors within your control (i.e. providing the wrong shipping address)2.
        Your order did not arrive due to exceptional circumstances outside the control of our {{ config('app.name') }} (i.e. not
        cleared by customs, delayed by a natural disaster).</p>

    <p>You can submit refund requests by sending a message on <a href="{{ url('contact-us') }}">Contact</a> Uspage or using your PayPal account.</p>

    <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
</section>
@endsection