@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Status order</li>
    </ul>

    <h2>Status your order</h2>
    <hr/>

    <form class="uk-form uk-grid" action="">
        <div class="uk-form uk-width-1-1">
            <label for="" >
                <strong>Your order key<span style="color: red" >*</span></strong>
                <input type="text" name="key" value="{{ $key or '' }}" class="uk-width-1-1 uk-margin-bottom" required placeholder="Your order key"/>
            </label>
        </div>
        <div>
            <button type="submit" class="uk-float-right uk-button uk-margin-top uk-button-primary" >Check</button>
        </div>
    </form>

    @if(isset($order) && !is_null($order))

    <div class="uk-grid uk-panel ">
        <div class="uk-width-large-1-5 uk-width-small-1-1">
            @include('common.orders-client', [ 'client' => json_decode($order->client, true), 'secret' => true ])
        </div>
        <div class="uk-width-large-4-5 uk-width-small-1-1">
            <div class="uk-overflow-container uk-panel uk-panel-box uk-padding" >
                <table class="uk-table uk-table-condensed ">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                        <th>Track</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    @isset($order->sales)
                        @foreach($order->sales as $product)
                            <tr>
                                <td>
                                    <div class="product-cart uk-grid uk-margin-remove">
                                        <div class="img uk-width-2-10" >
                                            <img
                                                src="{{ is_null($product['img']) ?
                                                                        Images::getImageByID(Product::getFirstImg($product['product_id']),'50x50') :
                                                                        Images::getImageByID($product['img'],'50x50') }}"
                                                style="" alt=""/>
                                        </div>
                                        <div class="product-info-cart uk-width-8-10">
                                            <p>
                                                <a href="{{ url('product/' . Product::whereId($product['product_id'])->pluck('slug')->first()) }}">
                                                    {{ $product['name'] }}
                                                </a>
                                            </p>
                                            <p class="uk-text-muted">
                                                {{ $product['variation'] }}
                                            </p>
                                        </div>
                                    </div>
                                </td>
                                <td >{{ $product['count'] }} pieces</td>
                                <td><strong>{{ $product['price']   }} {{ $product['currency'] or '' }} / one item</strong></td>
                                <td>{{ $product['track'] or 'expected' }}</td>
                                <td>{{ $product['details'] or 'no details' }}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
                <p><strong>Total price:</strong> {{ $order->price }}</p>
                <p><strong>Status order:</strong> {{ $order->status }}</p>
                <p><strong>Date order: </strong>{{ $order->created_at }}</p>
            </div>
        </div>
    </div>
    @endif
</section>
@endsection