@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Shipping & Delivery</li>
    </ul>

    <h2>Shipping & Delivery</h2>
    <hr/>

    <p>We are proud to offer international shipping services that currently operate in over 200 countries and islands world wide.
        Nothing means more to us than bringing our customers great value and service.
        We will continue to grow to meet the needs of all our customers, delivering a service beyond all expectation anywhere in the world.</p>

    <h4><strong>How do you ship packages?</strong></h4>
    <p>Packages from our warehouse in China will be shipped by ePacket or EMS depending on the weight and size of the product. Packages shipped from our US warehouse are shipped through USPS.</p>

    <h4><strong>Do you ship worldwide?</strong></h4>
    <p>Yes. We provide free shipping to over 200 countries around the world. However, there are some location we are unable to ship to. If you happen to be located in one of those countries we will contact you.</p>

    <h4><strong>What about customs?</strong></h4>
    <p>We are not responsible for any custom fees once the items have shipped. By purchasing our products, you consent that one or more packages may be shipped to you and may get custom fees when they arrive to your country.</p>

    <h4><strong>How long does shipping take?</strong></h4>
    <p>Shipping time varies by location. These are our estimates:</p>

    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped">
            <thead>
            <tr>
                <th>Location</th>
                <th>*Estimated Shipping Time</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>United States</td>
                <td>10-30 Business days</td>
            </tr>
            <tr>
                <td>Canada, Europe</td>
                <td>10-30 Business days</td>
            </tr>
            <tr>
                <td>Australia, New Zealand</td>
                <td>10-30 Business days</td>
            </tr>
            <tr>
                <td>Central &amp; South America</td>
                <td>15-30 Business days</td>
            </tr>
            </tbody>
        </table>
        <small class="uk-article-meta" >*This doesn’t include our 2-5 day processing time.</small>
    </div>

    <h4><strong>Do you provide tracking information?</strong></h4>
    <p>Yes, you will receive an email once your order ships that contains your tracking information.</p>

    <h4><strong>My tracking says “no information available at the moment”.</strong></h4>
    <p>For some shipping companies, it takes 2-5 business days for the tracking information to update on the system.</p>

    <h4><strong>Will my items be sent in one package?</strong></h4>
    <p>For logistical reasons, items in the same purchase will sometimes be	sent in separate packages, even if you've specified combined shipping.</p>

    <p>If you have any other questions, please contact us and we will do our best to help you out.</p>

    <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
</section>
@endsection