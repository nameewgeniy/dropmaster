@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Contact Us</li>
    </ul>

    <h2>Contact Us</h2>
    <hr/>
    @if(app('request')->input('send'))
        <div class="uk-alert uk-alert-success" data-uk-alert="">
            <a href="" class="uk-alert-close uk-close"></a>
            <p>Message sent successfully!</p>
        </div>
    @endif
    <form class="uk-form uk-grid" action="{{ url('contact-us/send') }}" >
        <div class="uk-form uk-width-1-2">
            <label for="" >
                <strong>Your Name<span style="color: red" >*</span></strong>
                <input type="text" name="name" class="uk-width-1-1 uk-margin-bottom" required placeholder="Your Name"/>
            </label>
            <label for="" class="uk-margin-bottom">
                <strong>Email<span style="color: red" >*</span></strong>
                <input type="email" name="email"  class="uk-width-1-1 uk-margin-bottom" required placeholder="Your Email"/>
            </label>
            <label for="" class="uk-margin-bottom">
                <strong>Message<span style="color: red" >*</span></strong>
                <textarea name="text"  required class="uk-width-1-1" id="" cols="30" rows="10"></textarea>
            </label>
            <div>
                <button type="submit" class="uk-float-right uk-button uk-margin-top" >Send Message</button>
            </div>
        </div>
        <div class="uk-width-1-2">
            <p>Have any questions or need to get more information about a product? Either way, you’re in the right spot.</p>
            <p><a href="mailto:{{ Setting::getSetting('email') }}" ><i class="uk-icon-envelope-o" ></i> {{ Setting::getSetting('email') }}</a></p>
            <p><a href="https://facebook.com/{{ Setting::getSetting('facebook') }}" target="_blank" ><i class="uk-icon-facebook" ></i> Like US</a></p>
            <p><a href="https://instagram.com/{{ Setting::getSetting('instagram') }}" target="_blank" ><i class="uk-icon-instagram" ></i> Find Us</a></p>
            <p><a href="https://twitter.com/{{ Setting::getSetting('twitter') }}" target="_blank" ><i class="uk-icon-twitter" ></i> Follow Us</a></p>
            <p><a href="https://pinterest.com/{{ Setting::getSetting('pinterest') }}" target="_blank" ><i class="uk-icon-pinterest-p" ></i> Add Us</a></p>
        </div>
    </form>

</section>
@endsection