@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Error payment</li>
    </ul>

    <i class="uk-icon-exclamation-circle uk-text-center" style="display: block; font-size: 110px; color: #F8981F"  ></i>
    <h1 class="uk-text-center" >Error payment...</h1>
    <div class="uk-text-center">
        <a href="{{ url('contact-us') }}" class="uk-button uk-button-primary">CONTACT US</a>
    </div>
    <br/>
</section>
@endsection