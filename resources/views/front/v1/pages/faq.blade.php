@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Frequently Asked Questions</li>
    </ul>

    <h2>Frequently Asked Questions</h2>
    <hr/>

    <h4><strong>How to search products?</strong></h4>
    <p>
        Search for products by entering the product name or keyword into the Search Bar at the top of any page.
        Try to enter a general description. The more keywords you use, the less products you will get in the results page.
        When you find a product you’re interested in, simply click the product name or the product image for more details.
    </p>

    <h4><strong>How are shipping costs calculated?</strong></h4>
    <p>
        Shipping costs are calculated based on shipping method (air, sea or land) and product weight / volume.
        Different shipping companies have different rates, so it’s best to check and compare which is most affordable
        and economical. For more details on how shipping costs are calculated, please contact us directly.
    </p>

    <h4><strong>What is Buyer Protection?</strong></h4>
    <p>Buyer Protection is a set of guarantees that enables buyers to shop with confidence on our website.</p>

    <p>You are protected when:</p>

    <ul>
        <li>The item you ordered did not arrive within the time promised by the seller.</li>
        <li>The item you received was not as described.</li>
        <li>The item you received that was assured to be genuine was fake.</li>
    </ul>

    <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
</section>
@endsection