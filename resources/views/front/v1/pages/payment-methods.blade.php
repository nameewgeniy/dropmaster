@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Payment methods</li>
    </ul>

    <h2>Payment methods</h2>
    <hr/>

    <p>
        Payments methods include PayPal and Credit cards.
    </p>

    <p>
        PayPal is a safer, easier way to send and receive money online. When you select PayPal as the payment method,
        you will be linked to the PayPal site where you can make payment.
    </p>

    <p>
        PayPal can be used to purchase items by Credit Card (Visa, MasterCard, Discover, and American Express), Debit
        Card, or E-check (i.e. using your regular Bank Account).
    </p>

    <p>
        1) After viewing your items on your shopping cart page, you can click and check out with PayPal.
        Then you will leave our site and enter PayPal’s website. <br/>
        2) You can sign in to your PayPal account, or you can create a new one if you haven’t got one. <br/>
        3) You can use the PayPal as you want according to the on-screen instructions.
    </p>

    <p>
        Usually, PayPal e-check will take 3-5 business days to be confirmed by PayPal.
    </p>

    <p>
        The reasons why we suggest you use PayPal:
        Payment is traceable. By using your PayPal account, you can trace the status of your payment.
        When you make payment for your order, you don’t need to use your credit card online (you can transfer directly
        from your bank account). When you use your credit card through PayPal, nobody will see your credit card number,
        which will minimize the risk of unauthorized use.
    </p>

    <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
</section>
@endsection