@extends('front.v1.index')
@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Track your order</li>
    </ul>

    <h2>Track your order</h2>
    <hr/>

    <div class="uk-form uk-grid" >
        <div class="uk-form uk-width-9-10">
            <label for="" >
                <input type="text" id="track" maxlength="50"  class="uk-width-1-1 uk-margin-bottom" placeholder="Track your order"/>
            </label>
        </div>
        <div class="uk-form uk-width-1-10">
            <button type="button" class="uk-float-right uk-button uk-button-primary" onclick="doTrack()" >Check</button>
        </div>
    </div>

    <div id="tracking"></div>

    <script type="text/javascript" src="//www.17track.net/externalcall.js"></script>
    <script type="text/javascript">
        function doTrack() {
            var num = document.getElementById("track").value;
            if(num===""){
                alert("Enter your number.");
                return;
            }
            YQV5.trackSingle({
                YQ_ContainerId:"tracking",       //Required, Specify the container ID of content host.
                YQ_Height:400,      //Optional, Specify tracking result height, max height 800px, default is filled the container.
                YQ_Fc:"0",       //Optional, Select carrier, default is auto-identify.
                YQ_Lang:"en",       //Optional, Specify UI language, default language is automatically detected based on browser settings.
                YQ_Num:num     //Required, Specify number need to be tracked.
            });
        }
    </script>

</section>
@endsection