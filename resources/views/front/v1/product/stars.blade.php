@isset($product->rate)
    <section class="stars">

        @for ($i = 0; $i < $product->rate; $i++)
            <span class="uk-icon-star"></span>
        @endfor

        @for ($i = 0; $i < 5 - $product->rate; $i++)
            <span class="uk-icon-star-o"></span>
        @endfor

        <span><strong>{{ $product->rate*20 }}%</strong> of buyers enjoyed this product!	<span class="orders">{{ $product->orders or '325' }} orders</span></span>
        @include('front.v1.include.share')
    </section>
    <hr/>
@endisset