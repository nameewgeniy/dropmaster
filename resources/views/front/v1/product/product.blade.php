@extends('front.v1.index')

    @push('styles')
        <link href="{{ asset('files/front/'. config('services.template.path') .'/css/fotorama.css') }}" rel="stylesheet">
        <link href="{{ asset('files/front/'. config('services.template.path') .'/css/notify.min.css') }}" rel="stylesheet">
    @endpush

    @push('scripts')
        <script src="{{ asset('files/front/'. config('services.template.path') .'/js/components/fotorama.js') }}"></script>
        <script src="{{ asset('files/front/'. config('services.template.path') .'/js/components/sticky.min.js') }}"></script>
        <script src="{{ asset('files/front/'. config('services.template.path') .'/js/components/notify.min.js') }}"></script>
        <script src="{{ asset('files/front/'. config('services.template.path') .'/js/components/lightbox.min.js') }}"></script>
        <script src="{{ asset('files/front/'. config('services.template.path') .'/js/attribute.js') }}"></script>
        <script src="{{ asset('files/front/'. config('services.template.path') .'/js/cart.js') }}"></script>

@endpush

@section('main')

    <script>
        var Product = {
            id          : "{{ $product->id or '' }}",
            currency    : "{{ $currency or '' }}",
            current     : "{{ $product->variations[0]->id }}",
            token       : "{{ csrf_token() }}"
        }
    </script>

    @include('front.v1.include.og')

    <section id="product" class="width1140" >
        <div class="uk-grid uk-margin-remove">
            <ul class="uk-breadcrumb uk-width-1-1">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="uk-active">
                        <a href="{{ url('category/' . $product->categories[0]->slug ) }}">{{ $product->categories[0]->name }}</a>
                </li>
                <li class="uk-text-muted">{{ Functions::more($product->title, 6) }}</li>
            </ul>
            <section class="uk-width-large-4-10 uk-width-medium-4-10 uk-width-1-1 gallery ">
                <div class="fotorama"
                     data-allowfullscreen="native"
                     data-click="true"
                     data-loop="true"
                     data-nav="thumbs"
                     data-thumbmargin="10"
                     data-uk-sticky="{boundary:true, top:10}"
                    >
                        @foreach($product->images as $image)
                            <img itemprop="image" src="{{ Images::getImageByID($image->id, '500x500') }}"
                                 data-full="{{ Images::getImageByID($image->id, 'full') }}" alt="">
                        @endforeach

                </div>
            </section>

            <section class="uk-width-large-6-10 uk-width-medium-6-10 uk-width-1-1 main-info">
                <h3>{{ $product->title }}</h3>

                @include('front.v1.product.stars')

                <p class="original-price" ><span>List price: </span><strike class="uk-text-muted" >{{ round($product->getPrice() + $product->getPrice() * 0.1, 2)  }} {{ $currency or '' }}</strike></p>
                <p class="price" ><span>Price: </span><span>{{ $product->getPrice() }} {{ $currency or '' }}</span></p>
                <p class="save" ><span>You save: </span><span>{{ round($product->getPrice() * 0.1,2) }} {{ $currency or '' }}</span></p>
                <hr/>

                @include('front.v1.product.options')

                <div class="number uk-form">
                    <span>Quantity:</span>
                    <span class="minus uk-button" >-</span>
                    <input type="text" id="quantity" value="1" />
                    <span class="plus uk-button" >+</span>
                    <span class="pieces uk-text-muted"> </span>
                </div>
                <p class="total-price">
                    <span>Total price: </span>
                    <span class="total-price-value">
                        <span class="uk-text-muted">
                            Depends on the product properties you select
                        </span>
                    </span>
                </p>
                <button class="uk-button-large add-to-cart" data-uk-modal="{bgclose:false}" onclick="ga('send', 'event', 'AddToCart', 'click'); return true;">Add to Cart</button>
                <p class="shipping"><strong>Shipping Cost:</strong>FREE WORLDWIDE SHIPPING</p>
                <hr/>

                <div class="buyer-protection">
                    <div class="icon-buyer"></div>
                    <p>Buyer Protection</p>
                    <p><strong>Full Refund </strong>if you don't receive your order</p>
                    <p><strong>Full or Partial Refund </strong>, if the item is not as described</p>
                </div>
            </section>
            <section class="description uk-width-1-1">
                <ul class="uk-tab" id="tab-buttons" data-uk-tab="{connect:'#description'}">
                    <li class="uk-active" ><a href="">Product Details</a></li>
                    <li><a href="">Feedback</a></li>
                    <li><a href="">Shipping and Free Returns</a></li>
                </ul>
                <ul id="description" class="uk-switcher uk-list">

                    <li>
                        <div class="uk-block uk-block-primary head-tab-text uk-margin-bottom"  >
                            <h3>PRODUCT DETAILS</h3>
                        </div>
                        {!! $product->text !!}
                    </li>
                    <li>@include('front.v1.product.comments')</li>
                    <li>@include('front.v1.product.shipping-text')</li>
                </ul>
            </section>
            <section class="related uk-width-1-1 uk-padding-remove">
                <div class="prod_head">
                    <div class="cat_title"><span class="">Premium Related Products</span></div>
                </div>
                @include('front.v1.product.related')
            </section>
        </div>
    </section>
    <section class="modal-cart">
        <div class="uk-modal" id="modal-cart" >
            <div class="uk-modal-dialog">
                <div class="uk-modal-header"><h3>A new item has been added to your Shopping Cart</h3></div>
                <a class="uk-button" href="/cart">View Shopping Cart</a>
                <a class="uk-button uk-modal-close" href="#">Continue Shopping</a>
            </div>
        </div>
    </section>

@endsection