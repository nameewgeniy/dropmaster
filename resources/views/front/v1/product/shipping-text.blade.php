<div class="uk-block uk-block-primary head-tab-text" >
    <h3>SHIPPING AND DELIVERY</h3>
</div>

<p>We are proud to offer international shipping services that currently operate in over 200 countries and islands world wide.
    Nothing means more to us than bringing our customers great value and service.
    We will continue to grow to meet the needs of all our customers, delivering a service beyond all expectation anywhere in the world.</p>

<h4><strong>How do you ship packages?</strong></h4>
<p>Packages from our warehouse in China will be shipped by ePacket or EMS depending on the weight and size of the product. Packages shipped from our US warehouse are shipped through USPS.</p>

<h4><strong>Do you ship worldwide?</strong></h4>
<p>Yes. We provide free shipping to over 200 countries around the world. However, there are some location we are unable to ship to. If you happen to be located in one of those countries we will contact you.</p>

<h4><strong>What about customs?</strong></h4>
<p>We are not responsible for any custom fees once the items have shipped. By purchasing our products, you consent that one or more packages may be shipped to you and may get custom fees when they arrive to your country.</p>

<h4><strong>How long does shipping take?</strong></h4>
<p>Shipping time varies by location. These are our estimates:</p>

<div class="uk-overflow-container">
    <table class="uk-table uk-table-striped">
        <thead>
            <tr>
                <th>Location</th>
                <th>*Estimated Shipping Time</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>United States</td>
                <td>10-30 Business days</td>
            </tr>
            <tr>
                <td>Canada, Europe</td>
                <td>10-30 Business days</td>
            </tr>
            <tr>
                <td>Australia, New Zealand</td>
                <td>10-30 Business days</td>
            </tr>
            <tr>
                <td>Central &amp; South America</td>
                <td>15-30 Business days</td>
            </tr>
        </tbody>
    </table>
    <small class="uk-article-meta" >*This doesn’t include our 2-5 day processing time.</small>
</div>

<h4><strong>Do you provide tracking information?</strong></h4>
<p>Yes, you will receive an email once your order ships that contains your tracking information.</p>

<h4><strong>My tracking says “no information available at the moment”.</strong></h4>
<p>For some shipping companies, it takes 2-5 business days for the tracking information to update on the system.</p>

<h4><strong>Will my items be sent in one package?</strong></h4>
<p>For logistical reasons, items in the same purchase will sometimes be	sent in separate packages, even if you've specified combined shipping.</p>

<p>If you have any other questions, please contact us and we will do our best to help you out.</p>

<div class="uk-block uk-block-primary head-tab-text " style="margin-top: 20px"  >
    <h3>REFUNDS & RETURNS POLICY</h3>
</div>

<h4><strong>Order cancellation</strong></h4>
<p>All orders can be cancelled until they are shipped. If your order has been paid and you need to make a change or cancel an order, you must contact us within 12 hours. Once the packaging and shipping process has started, it can no longer be cancelled.</p>

<h4><strong>Refunds</strong></h4>
<p>Your satisfaction is our #1 priority. Therefore, if you’d like a refund you can request one no matter the reason.</p>

<p>If you did <strong>not</strong> receive the product within the guaranteed time(45 days not including 2-5 day processing) you can request a refund or a reshipment.</p>

<p>If you received the wrong item you can request a refund or a reshipment.</p>

<p>If you do not want the product you’ve receive you may request a refund but you must return the item at your expense and the item must be unused.</p>

<ul>
    <li>- Your order did not arrive due to factors within your control (i.e. providing the wrong shipping address)</li>
    <li>- Your order did not arrive due to exceptional circumstances outside the control of <span class="uk-blend-color" >{{ config('app.name') }}</span> (i.e. not cleared by customs, delayed by a natural disaster).</li>
    <li>- Other exceptional circumstances outside the control of <a href="{{ url('/') }}">{{ config('app.name') }}</a></li>
</ul>

<small class="uk-article-meta" >*You can submit refund requests within 15 days after the guaranteed period for delivery (45 days) has expired. You can do it by sending a message on Contact Us page</small>

<p>If you are approved for a refund, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within 14 days.</p>

<h4><strong>Exchanges</strong></h4>
<p>If for any reason you would like to exchange your product, perhaps for a different size in clothing. You must contact us first and we will guide you through the steps.</p>

<p>Please do not send your purchase back to us unless we authorise you to do so.</p>
