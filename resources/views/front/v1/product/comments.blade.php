@isset($product->comments)
    <div class="uk-block uk-block-primary head-tab-text uk-margin-bottom"  >
        <h3>PRODUCT FEEDBACK</h3>
    </div>
    <ul class="uk-comment-list">
        @foreach($product->comments as $comment)
            <li>
                <article class="uk-comment">
                    <header class="uk-comment-header uk-margin-bottom-remove">
                        <h4 class="uk-comment-title">
                            <strong>Name: </strong> {{ $comment->name }}
                        </h4>
                        <div class="uk-comment-meta">Country: {{ $comment->country }} | {{ $comment->date }} </div>
                    </header>
                    <div class="uk-comment-body">
                        <h4>{{ $comment->comment }}</h4>
                        @if(!is_null( $comment->images ))
                            <hr/>
                            @foreach( json_decode($comment->images) as $image )
                                <a href="{{ $image }}" class="uk-thumbnail "
                                   style="background-image: url({{ $image }})"
                                   data-uk-lightbox="{group:'{{ $comment->date }}'}">
                                </a>
                            @endforeach
                        @endif
                    </div>
                </article>
            </li>
        @endforeach
    </ul>
@endisset