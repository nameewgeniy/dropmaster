@if(isset($options) && !empty($options))
    <div class='options'>
    @foreach($options as $name => $option)
        <p>{{ $name }}</p>
        <div data-uk-button-radio>
        @foreach($option['obj'] as $item)

            <label class="uk-button"
                   style="{!! !is_null($item['img'])? 'background: url("' . Images::getImageByID($item['img']). '")' : '' !!}"
            >{{ $item['value'] }}
                <input type="radio" name="{{ $name }}"  value="{{ $item['value'] }}"/>
            </label>
        @endforeach
            <div class="uk-alert uk-alert-warning">Please select a <span>{{ str_replace(':', '', $name) }}</span></div>
        </div>
    @endforeach
    </div>
    <hr/>
@endif
<!--<label class="uk-button img" style="background: url('')" >-->



