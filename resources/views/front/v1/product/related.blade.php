<div class="uk-grid uk-grid-medium list-product" style="padding-top: 20px">
    @isset($related)
        @include('front.v1.include.products', ['products' => $related])
    @endisset
</div>