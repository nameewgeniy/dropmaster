<div data-uk-slideset="{default: 3, animation: 'fade', duration: 300, autoplayInterval: 7000, autoplay: true}">
    <ul class="uk-grid uk-slideset">
        @foreach( $bestsellers as $product )
            <li>
                <div class="uk-thumbnail uk-thumbnail-medium"  >
                        <div class="img-product" style="background-image: url({{ Images::getImageByID($product->images[0]->id) }})" ></div>
                        <div class="uk-thumbnail-caption uk-grid">
                            <div class="uk-width-1-2"><p class="caption-price"><strong>Price: </strong>{{ $product->getBestPrice() }} {{ $currency or Setting::getCurrency() }}</p></div>
                            <div class="uk-width-1-2"><a class="uk-button caption-button" href="{{ url('product/' . $product->slug) }}  ">More</a></div>
                        </div>
                </div>
            </li>
        @endforeach
    </ul>
    <!--<ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>-->
</div>
