@if( ($bestsellers = Categories::whereSlug($category)->first()) )
    <div class="prod_head">
        <div class="cat_title"><span class="">{{ $bestsellers->name }}</span></div>
    </div>
    <div class="uk-grid uk-grid-medium list-product" style="padding-top: 20px">
        @if(isset($take))
            @include('front.v1.include.products', ['products' => $bestsellers->products->where('status', 1)->take($take)])
        @else
            @include('front.v1.include.products', ['products' => $bestsellers->products->where('status', 1)->take(4)])
        @endif
    </div>
@endif