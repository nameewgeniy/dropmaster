@extends('front.v1.index')

@section('main')

<section id="page" class="width1140" >

    <ul class="uk-breadcrumb uk-width-1-1">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="uk-text-muted">Subscribe</li>
    </ul>

    <i class="uk-icon-check-circle uk-text-center" style="display: block; font-size: 110px; color: #6CB15E"  ></i>
    <h1 class="uk-text-center" >Thank you for subscribing to our newsletter!</h1>
    <h2 class="uk-text-center" >You have been successfully added to our mailing list,<br/> keeping you up-to-date with our latest news, sales and coupons</h2>
    <h2 class="uk-text-center">Your promo <strong style="color: #eea12d; font-weight: bold">save10</strong>. Use it in cart!</h2>
    <div class="uk-text-center">
        <a href="{{ url('/') }}" class="uk-button uk-button-primary">GO TO HOME</a>
    </div>
    <br/>
</section>

@endsection

