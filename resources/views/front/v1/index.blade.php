@extends('front.v1.app')
@section('content')
    <header class="home" >
        <section class="buy-it">
            <h3 class="uk-text-center" >Subscribe <span>now</span> and get sale up to <span>10%</span> off all items!</h3>
            <button class="uk-animation-shake uk-button-success uk-button uk-align-center uk-button-large"
                    data-uk-modal="{target: '#m-subscribe'}" >Subscribe</button>
        </section>
        <!-- This is the modal -->
        <div id="m-subscribe" class="uk-modal">
            <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close"></a>
                <form class="uk-form uk-margin-remove uk-padding uk-clearfix" action="{{ route('subscribe') }}" id="subscribe">
                    <input type="text" name="subscribe" class="uk-width-1-1" placeholder="Please enter you email">
                    <button class="uk-button uk-button-success uk-float-right uk-margin">Subscribe</button>
                </form>
            </div>
        </div>

        <section class="menu-header">
            <nav class="uk-navbar width1140 no-background no-border ">
                <ul class="uk-navbar-nav uk-visible-small">
                    <li>
                        <a href="#side-menu" data-uk-offcanvas><i class="uk-icon-bars" ></i></a>
                    </li>
                    <li>
                        <a href="#side-categories" data-uk-offcanvas>Categories</a>
                    </li>
                </ul>
                <ul class="uk-navbar-nav uk-visible-large">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('shipping-delivery') }}">Shipping & Delivery</a></li>
                    <li><a href="{{ url('returns-policy') }}">Returns Policy</a></li>
                    <li><a href="{{ url('faq') }}">FAQ</a></li>
                    <li><a href="{{ url('order/status') }}">Check order</a></li>
                    <li><a href="{{ url('sitemap.xml') }}">Sitemap</a></li>
                    <li><a href="{{ route('track') }}">Tracking</a></li>
                    <li>
                        <a href="#side-categories" data-uk-offcanvas>Categories</a>
                    </li>
                </ul>
                <div class="uk-navbar-flip uk-visible-large">
                    <ul class="uk-navbar-nav">
                        <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                    </ul>
                </div>
            </nav>

            <div id="side-menu" class="uk-offcanvas">
                <div class="uk-offcanvas-bar">
                    <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('shipping-delivery') }}">Shipping & Delivery</a></li>
                        <li><a href="{{ url('returns-policy') }}">Returns Policy</a></li>
                        <li><a href="{{ url('faq') }}">FAQ</a></li>
                        <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                        <li><a href="{{ url('order/status') }}">Check order</a></li>
                        <li><a href="{{ route('track') }}">Tracking</a></li>
                    </ul>
                </div>
            </div>

            <div id="side-categories" class="uk-offcanvas">
                <div class="uk-offcanvas-bar">
                    <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon">
                        @foreach( App\Models\Category::all() as $cat)
                        <li><a href="{{ url('category/' . $cat->slug) }}">{{ $cat->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>
        <section class="second-header-menu ">
            <div class="width1140 uk-grid">
                <div class="uk-width-large-1-6 uk-visible-large uk-padding-remove" style="position: relative">
                    <a class="uk-navbar-brand sky logo uk-display-block" href="{{ url('/') }}">
                        <p class="logo-first">MyPick</p><br/>
                        <p class="logo-second">Store</p>
                    </a>
                </div>
                <div class="uk-width-large-3-6 uk-width-medium-4-6 uk-width-2-3">
                    <form class="uk-form uk-margin-remove" action="{{ url('search') }}">
                        <input type="text" name="skey" value="{{ $skey or '' }}" placeholder="Search">
                        <button class="uk-button uk-button-default uk-icon-search"></button>
                    </form>
                </div>
                <div
                    class="uk-width-large-1-6 uk-visible-large uk-text-center"
                    style="
                    background: url({{ asset('files/front/'. config('services.template.path') .'/images/mcafee.png') }}) no-repeat center;
                    background-size: contain;
                    ">
                </div>
                <a class="uk-width-large-1-6 uk-width-medium-2-6 uk-width-1-3 uk-padding-remove-right uk-grid" href="{{ url('/cart') }}" id="cart-header">
                    <div class="uk-width-1-2">
                        <span class="icon-cart ion-ios-cart-outline"  ></span>
                    </div>
                    <div class="uk-width-1-2 uk-grid">
                        <div class="uk-width-1-1 uk-padding-remove uk-margin-remove">
                            <span class="header-cart-count">0</span>
                        </div>
                        <div class="uk-width-1-1 uk-hidden-small uk-padding-remove uk-margin-remove">Cart</div>
                    </div>
                </a>
            </div>
        </section>
    </header>

    <section class="content-site">
        <section class="errors width1140 uk-margin-top">
            @if( isset($errors))
                @foreach( $errors->all() as $error )
                    <div class="uk-alert uk-alert-dancer" data-uk-alert="">
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p>{{ $error }}</p>
                    </div>
                @endforeach
            @endif
        </section>

        @yield('main')
    </section>

    <footer style="position: relative">

        <a href="#body"
           class="uk-button uk-button-primary"
           data-uk-scrollspy="{cls:'uk-animation-slide-right', repeat: true}"
           style="position: absolute; right: 50px; bottom: 260px" data-uk-smooth-scroll>
            <i class="uk-icon-angle-double-up" style="font-size: 40px"></i>
        </a>

        <section class="payment">
            <div class="uk-grid width1140">
                <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
                    Payment Methods:
                    <img src="{{ asset('/files/front/v1/images/payments.png') }}" alt=""/>
                </div>
                <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
                    Delivery Methods:
                    <img src="{{ asset('/files/front/v1/images/delivery.png') }}" alt=""/>
                </div>
            </div>
        </section>
        <section class="footer-menu">
            <div class="width1140 uk-grid ">
                <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1 uk-grid uk-grid-medium uk-padding-remove">
                    <div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-3">
                        <ul class="uk-list">
                            <li>COMPANY INFO</li>
                            <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                            <li><a href="{{ url('terms-and-conditions') }}">Terms and Conditions</a></li>
                        </ul>
                    </div>
                    <div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-3">
                        <ul class="uk-list">
                            <li>PURCHASE INFO</li>
                            <li><a href="{{ url('payment-methods') }}">Payment methods</a></li>
                            <li><a href="{{ url('shipping-delivery') }}">Shipping & Delivery</a></li>
                            <li><a href="{{ url('returns-policy') }}">Returns Policy</a></li>
                        </ul>
                    </div>
                    <div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-3">
                        <ul class="uk-list">
                            <li>CUSTOMER SERVICE</li>
                            <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                            <li><a href="{{ url('faq') }}">Frequently Asked Questions</a></li>
                        </ul>
                    </div>
                </div>
                <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-">
                    <div class="social">

                    </div>
                    <div>
                        <p style="color: #fff; padding-top: 3px">STAY UP TO DATE</p>
                        <form class="uk-form uk-margin-remove" action="{{ route('subscribe') }}" id="subscribe">
                            <input type="text" name="subscribe" placeholder="Please enter you email">
                            <button class="uk-button uk-button-default ">Subscribe</button>
                        </form>
                    </div>

                </div>
            </div>
        </section>
        <section class="copyright">
            <div class="width1140 uk-text-center">
                <span style="color: #868686">© Copyright 2017. All Rights Reserved</span><br/>
                <span style="color: #c8c8c8">{{ Setting::getSetting('address') }}</span><br/>
                <a class="uk-text-muted" href="https://www.freelancer.com/u/Ewgeni" ><dfn>by Ewgeniy Nakapyuk</dfn></a><br/>
            </div>
        </section>

    </footer>

@endsection