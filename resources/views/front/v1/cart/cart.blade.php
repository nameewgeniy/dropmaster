@extends('front.v1.index')

@push('scripts')
    <script src="{{ asset('files/front/'. config('services.template.path') .'/js/validate.js') }}"></script>
    <script src="{{ asset('files/front/'. config('services.template.path') .'/js/cart.js') }}"></script>

    <script src="{{ asset('files/front/'. config('services.template.path') .'/js/components/sticky.min.js') }}"></script>
    <script>
        var validator = new FormValidator('cart-form', [{
                name: 'client[email]',
                display: 'Email',
                rules: 'required'
            },
            {
                name: 'client[name]',
                display: 'Name',
                rules: 'required'
            },
            {
                name: 'client[address]',
                display: 'Address',
                rules: 'required'
            },
            {
                name: 'client[city]',
                display: 'City',
                rules: 'required'
            },
            {
                name: 'client[country]',
                display: 'Country',
                rules: 'required'
            },
            {
                name: 'client[region]',
                display: 'Region',
                rules: 'required'
            }
            ], function(errors, event) {
                if (errors.length > 0) {
                    var errorString = '';

                    for (var field in this.fields) {
                        jQuery('[name="'+ this.fields[field].name +'"]').removeClass('uk-form-danger');
                    }

                    for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
                        jQuery(errors[i].element).addClass('uk-form-danger');
                    }
                }
        });
    </script>
@endpush

@section('main')

    <section id="cart" class="width1140 ">
        @if( isset($products) && !empty($products) )
            <form class="uk-grid uk-form uk-form-stacked" name="cart-form" method="post" action="{{ url('pay') }}">
                <div class="uk-width-1-1">
                    <h1 class="cart-head">Cart</h1>
                </div>
                <div class="uk-width-large-7-10 uk-width-small-1-1 uk-margin-top">
                    <div class="uk-width-1-1 items">
                        <div class="uk-overflow-container uk-panel">
                            <table class="uk-table uk-table-condensed ">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <input type="hidden" name="items[{{ $product['id'] }}]" value="{{ $product['quantity'] }}"/>
                                    <tr>
                                        <td>
                                            <div class="product-cart uk-grid uk-margin-remove">
                                                <div class="img uk-width-4-10" >
                                                    <img
                                                        src="{{ is_null($product['img']) ? Images::getImageByID(Product::getFirstImg($product['product_id'])) : Images::getImageByID($product['img']) }}"
                                                        style="" alt=""/>
                                                </div>
                                                <div class="product-info-cart uk-width-6-10">
                                                    <p>
                                                        <a href="{{ url('product/' . Product::whereId($product['product_id'])->pluck('slug')->first()) }}">
                                                            {{ Product::whereId($product['product_id'])->pluck('title')->first() }}
                                                        </a>
                                                    </p>
                                                        <p class="uk-text-muted">
                                                            {{ $product['name'] }}
                                                        </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td >{{ $product['quantity'] }} pieces</td>
                                        <td><strong>{{ $product['price'] * $product['quantity'] }} {{ Setting::getCurrency() }}</strong></td>
                                        <td><span class="uk-icon-trash-o delete-product-cart" onclick="Cart.remove({{ $product['id'] }}, this)"  ></span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="uk-panel uk-panel-box uk-grid uk-margin-remove " >

                            <div class="uk-width-1-2 " style="padding-left: 0">
                                <fieldset data-uk-margin class="uk-align-left " style="padding-top: 34px">
                                    <input type="text" name="promocode" value="{{ app('request')->input('promocode') }}"  class="" placeholder="Enter promo code">
                                    <button class="uk-button uk-button-success" type="button" onclick="Cart.setPromocode(this)" >Apply</button>
                                </fieldset>
                            </div>
                            <div class="uk-width-1-2">
                                <p class="uk-text-right uk-margin-remove">Total pieces: {{ $final_count or '' }} </p>
                                <p class="uk-text-right uk-margin-remove">You save: {{ $discount or '0.00' }} {{ Setting::getCurrency() }}</p>
                                <p class="uk-text-right uk-margin-remove">Shipping: {{ $shipping or '0.00' }} {{ Setting::getCurrency() }}</p>
                                <p class="uk-text-right"><strong>Total price: {{ $final_price or '' }} {{ Setting::getCurrency() }}</strong></p>
                            </div>

                        </div>

                        <legend class="uk-margin-top" >Please fill in your shipping details:</legend>

                        <section class="fields uk-grid">

                            <div class="uk-width-1-2">
                                <label class="uk-form-label" for="">Email:<span style="color: red">*</span></label>
                                <input type="email" required name="client[email]" value="{{ old('client[email]') }}"  class="uk-width-1-1"/>
                            </div>

                            <div class="uk-width-1-2">
                                <label class="uk-form-label" for="">Name:<span style="color: red">*</span></label>
                                <input type="text" required name="client[name]" value="{{ old('client[name]') }}"  class="uk-width-1-1"/>
                            </div>

                            <div class="uk-width-1-1 uk-margin">
                                <hr />
                            </div>

                            <div class="uk-width-2-3 uk-margin-bottom">
                                <label class="uk-form-label " for="">Address:<span style="color: red">*</span></label>
                                <input type="text" required name="client[address]" value="{{ old('client[address]') }}" class="uk-width-1-1"/>
                            </div>

                            <div class="uk-width-1-3 uk-margin-bottom">
                                <label class="uk-form-label" for="">Apt, suite, etc. (optional):</label>
                                <input type="text"  name="client[etc]" value="{{ old('client[etc]') }}" class="uk-width-1-1"/>
                            </div>

                            <div class="uk-width-2-3 uk-margin-bottom">
                                <label class="uk-form-label" for="">City:<span style="color: red">*</span></label>
                                <input type="text" required name="client[city]" value="{{ old('client[city]') }}" class="uk-width-1-1"/>
                            </div>

                            <div class="uk-width-1-3 uk-margin-bottom">
                                <label class="uk-form-label" for="">Country:<span style="color: red">*</span></label>
                                <select name="client[country]" required class="uk-width-1-1" onchange="Cart.changeCountry(this)">
                                        <option value="0" selected ></option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->code }}" >{{ $country->name }}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="uk-width-1-3 uk-margin-bottom" style="position: relative">
                                <label class="uk-form-label" for="">State/Region:<span style="color: red">*</span></label>
                                <div class="uk-autocomplete uk-form uk-width-1-1" id="region" >
                                    <input type="text" required class="uk-width-1-1" name="client[region]">
                                </div>
                            </div>

                            <div class="uk-width-1-3 uk-margin-bottom">
                                <label class="uk-form-label" for="">Postal code:<span style="color: red">*</span></label>
                                <input type="text" required name="client[code]" value="{{ old('client[code]') }}"  class="uk-width-1-1"/>
                            </div>

                            <div class="uk-width-1-3 uk-margin-bottom">
                                <label class="uk-form-label" for="">Phone:</label>
                                <input type="text" name="client[phone]" value="{{ old('client[phone]') }}"  class="uk-width-1-1"/>
                            </div>

                            <div class="uk-width-1-1 uk-margin-bottom">
                                <label class="uk-form-label" for="">Additional details:</label>
                                <textarea name="client[details]"  id="" class="uk-width-1-1"
                                          placeholder="Please leave any questions or wishes regarding your order here"
                                          cols="30"
                                          rows="5">{{ old('client[details]') }}</textarea>
                            </div>

                            <div class="uk-width-1-1 ">
                                <div class="uk-panel uk-panel-box ">

                                        <h3 class="uk-text-right uk-margin-remove"><strong style="color: #FF8D38">Total price: {{ $final_price }} {{ Setting::getCurrency() }}</strong></h3>
                                </div>

                                <button class="uk-button-large uk-float-right uk-margin uk-button-success uk-button"
                                        id="pay"
                                        type="submit" onclick="ga('send', 'event', 'Pay', 'click');" >Proceed to Pay</button>
                            </div>

                        </section>

                    </div>
                </div>
                <div class="uk-width-large-3-10 uk-width-small-1-1 uk-margin-top" >
                    <div data-uk-sticky="{boundary:true, top:10}" >
                        <div class="uk-panel uk-panel-box uk-margin-bottom">
                            <h3 class="uk-panel-title ">SHOP WITH CONFIDENCE</h3>
                            <p>SHOPPING ON DROPMASTER.ORG IS SAFE AND SECURE. GUARANTEED!</p>
                            <p>All information is encrypted and transmitted without risk using a Secure Sockets Layer (SSL) protocol.</p>
                        </div>
                        <div class="uk-panel uk-panel-box uk-margin-top-remove uk-margin-bottom">
                            <h3 class="uk-panel-title">PRIVACY POLICY</h3>
                            <p>{{ config('app.name') }} respects your privacy. We don't rent or sell your personal information to anyone.</p>
                            <a href="{{ url('privacy-policy') }}">Read our Privacy Policy »</a>
                        </div>
                        <div class="uk-panel uk-panel-box uk-margin-top-remove uk-margin-bottom">
                            <h3 class="uk-panel-title">BUYER PROTECTION</h3>
                            <ul>
                                <li>Full Refund if you don't receive your order</li>
                                <li>Refund or Keep items not as described</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        @else
            <div class="uk-grid uk-margin-remove " >
                <div class="uk-width-1-1">
                    <h1 class="cart-head">Cart</h1>
                </div>
                <div class="uk-width-1-1 uk-margin">
                    <div class="uk-alert uk-alert-warning"><h3 class="uk-text-center" >You cart is empty...</h3></div>
                </div>
            </div>
        @endif

    </section>
@endsection