<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon shortcut" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">
    <meta name="p:domain_verify" content="0614c4a4eb2c4fbebe472ef73e73c428"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="{{ $description or '' }}">
    <meta name="keywords" content="{!! $keywords or '' !!}">
    <title>{{ $title or config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('files/front/'. config('services.template.path') .'/css/uikit.css') }}" rel="stylesheet">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    @stack('styles')
    <link href="{{ asset('files/front/'. config('services.template.path') .'/css/app.css') }}" rel="stylesheet">

</head>

<body class="uk-height-viewport" id="body">

@yield('content')

<!-- Scripts -->
<script src="{{ asset('files/front/'. config('services.template.path') .'/js/uikit.min.js') }}"></script>
<script src="{{ asset('files/front/'. config('services.template.path') .'/js/jquery.cookie.js') }}"></script>
@stack('scripts')
<script src="{{ asset('files/front/'. config('services.template.path') .'/js/base.js') }}"></script>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="https://v2.zopim.com/?5GxsgyYYBC51gLDjil1fZgu4GzbGchN2";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

{!! Setting::getSetting('scripts') !!}

</body>
</html>
