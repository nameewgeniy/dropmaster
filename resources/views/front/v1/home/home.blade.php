@extends('front.v1.index')
@push('scripts')
    <script src="{{ asset('files/front/'. config('services.template.path') .'/js/components/slideshow.min.js') }}"></script>
    <script src="{{ asset('files/front/'. config('services.template.path') .'/js/components/slideset.min.js') }}"></script>
@endpush

@section('main')

<section id="slide-home" >

    <div class="width1140 uk-grid">
        <div class="categories-sidebar
        uk-width-large-2-10
        uk-visible-large
        ">
            @include('front.v1.category.categories',['count' => 10])
        </div>
        <div class="slide-home-main
        uk-width-large-8-10
        uk-width-medium-1-1
        uk-width-small-1-1
        uk-margin-remove-right" >

            @include('front.v1.include.gallery')
        </div>
    </div>

</section>
<section id="features">
    <div class="prod_head width1140">
        <div class="cat_title" style="text-align: center"><span class="">WHY SHOP WITH US</span></div>
    </div>
    <div class="uk-grid width1140">
        <div class="
          uk-width-large-1-4
          uk-width-medium-2-4
          uk-width-small-1-2
         items uk-text-center">
            <span class="ion-ios-heart-outline" ></span>
            <p class="title-feature">700+ Clients Love Us!</p>
            <p>We offer best service and great prices on high quality products</p>
        </div>
        <div class="
        uk-width-large-1-4
          uk-width-medium-2-4
          uk-width-small-1-2 items uk-text-center">
            <span class="ion-card" ></span>
            <p class="title-feature">100% Safe Payment</p>
            <p>Buy with confidence using the world’s most popular and secure payment methods</p>
        </div>
        <div class="
         uk-width-large-1-4
          uk-width-medium-2-4
          uk-width-small-1-2
          items uk-text-center">
            <span class="ion-plane" ></span>
            <p class="title-feature">Shipping to 185 Countries</p>
            <p>Our store operates worldwide and you can enjoy free delivery of all orders</p>
        </div>
        <div class="
         uk-width-large-1-4
          uk-width-medium-2-4
          uk-width-small-1-2
          items uk-text-center">
            <span class="ion-ios-checkmark-outline" ></span>
            <p class="title-feature">1000+ Successful Deliveries</p>
            <p>Our Buyer Protection covers your purchase from click to delivery</p>
        </div>
    </div>
</section>
<section id="best-seller" class="width1140">

    @include('front.v1.product.category', ['category' => 'bestsellers', 'take' => 8])

    @if( ($take = Setting::getSetting('home_cat')) )
        @foreach( $categories->take($take) as $cat)
            @include('front.v1.product.category', ['category' => $cat->slug])
        @endforeach
    @endif

</section>
<section class="blog-social width1140 uk-grid uk-grid-medium">
        <div class="prod_head uk-width-1-1">
            <div class="cat_title"><span class="">JOIN US ON SOCIAL MEDIA</span></div>
        </div>

        <div class="uk-width-large-1-3 uk-width-small-1-1 s-box"  >
            @include('front.v1.social.pinterest')

        </div>

        <div class="uk-width-large-1-3 uk-width-small-1-1 s-box">
            @include('front.v1.social.twitter')
        </div>

        <div class="uk-width-large-1-3 uk-width-small-1-1 s-box">
            @include('front.v1.social.facebook')
        </div>
</section>

@endsection