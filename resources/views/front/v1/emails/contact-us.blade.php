<body>
    <h3>Message from Contact Us</h3>
    <table style="border-style: solid;margin: 20px 0;padding: 10px;border-width: thin; width: 100%">
        <thead>
            <tr style="border-style: solid;border-width: thin;">
                <th>Name</th>
                <th>Email</th>
                <th>Message</th>
            </tr>
        </thead>
        <tbody>
             <tr>
                 <td>{{ $request['name'] }}</td>
                 <td>{{ $request['email'] }}</td>
                 <td>{{ $request['text'] }}</td>
             </tr>
        </tbody>
    </table>
</body>