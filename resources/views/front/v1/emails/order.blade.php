<body>
    <h3>You place an order on {{ config('app.name') }}</h3>
    <h4>Check you order status <a href="{{ url('order/status?key=') }}{{ $order->key or '' }}">{{ $order->key or '' }}</a></h4>
    <table style="border-style: solid;margin: 20px 0;padding: 10px;border-width: thin; width: 100%">
        <thead>
            <tr style="border-style: solid;border-width: thin;">
                <th>Name</th>
                <th>Pieces</th>
                <th>Price</th>
                <th>Payment status</th>
                <th>Date order</th>
                <th>Track ID</th>
            </tr>
        </thead>
        <tbody>

            @foreach($order->sales as $product)
                <tr >
                    <td style="border-bottom: 1px solid #E5E5E5; max-width: 500px; padding: 10px">
                        <img src="{{ is_null($product['img']) ?
                                                        Images::getImageByID(Product::getFirstImg($product->product_id),'50x50') :
                                                        Images::getImageByID($product->img,'50x50') }}"
                            style="" alt=""/>
                        <a href="{{ url('product/' . Product::whereId($product->product_id)->pluck('slug')->first()) }}">
                            {{ $product->name }} / {{ $product->variation }}
                        </a>
                    </td>
                    <td style="border-bottom: 1px solid #E5E5E5;padding: 10px; text-align: center">{{ $product->count }}</td>
                    <td style="border-bottom: 1px solid #E5E5E5;padding: 10px; text-align: center">{{ $product->price   }} {{ $product->currency  }} / one item</td>
                    <td style="border-bottom: 1px solid #E5E5E5;padding: 10px; text-align: center">{{ $order->status }}</td>
                    <td style="border-bottom: 1px solid #E5E5E5;padding: 10px; text-align: center">{{ $order->created_at }}</td>
                    <td style="border-bottom: 1px solid #E5E5E5;padding: 10px; text-align: center">{{ $product->track }}</td>
                </tr>
            @endforeach

             <tr>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td><strong>Total price: {{ $order->price }}</strong></td>
             </tr>
        </tbody>
    </table>
    <div class="uk-width-1-5">
        @include('common.orders-client', [ 'client' => json_decode($order->client, true) ])
    </div>
</body>